<?php

// To Select Active Item Menu
function activeCheck(string $routesName)
{
    if(request()->routeIs($routesName)){
        return 'act-link';
    }
    return '';
}
// Push Lang to session
function getAllLang(){

    if(!session()->has('webLangs')){
        $data = [];
        $langs = App\Models\Language::where('is_active',1)->get();
        foreach ($langs as $lang){
            $data[$lang->code] = ['name'=>$lang->name,'default' => $lang->default];
        }
        session()->push('webLangs',$data);
        session()->save();
    }

    return session()->get('webLangs');
}
//push Currencies to Session
function websiteCurrenciesSet(){
    $data = [];
    $Currencies = \App\Models\Currency::where('is_active',1)->get();
    foreach ($Currencies as $Currency){
        $data[$Currency->code] = ['key'=>$Currency->key,'default' => $Currency->default,'icon'=>$Currency->icon];
    }
    Session::push('currencies',$data);
    Session::save();
    return $data;
}
//Tag Filter
function stringFiltter(string $str){
    $str = str_replace('[', '', $str);
    $str = str_replace(']', '',$str);
    $str = str_replace('{', '', $str);
    $str = str_replace('}', '', $str);
    $str = str_replace('"', '', $str);
    $str = str_replace('value:', '', $str);
    return $str;
}
function getStringFromArray(array $array){
    try {
        $string = '';
        foreach($array as $arr)
        {
            $string .= $arr.',';
        }
        return substr($string,0,-1);
    }catch (\Exception $ex){
       return '';
    }
}
// Get getCategory Type
function getCategory(){
    if(!isset(session()->get('categories')[App::getLocale()])){
        $data[App::getLocale()] = [];
        $categories = App\Models\Category::where('is_active',1)->get();
        foreach ($categories as $category){
            $data[App::getLocale()][$category->id] = ['title'=>$category->title,'slug'=>slugfy($category->title),'tags' => $category->tags,'description' => $category->description,'type'=>$category->type];
        }
        session()->put('categories',$data);
        session()->save();
    }
    return (isset(session()->get('categories')[App::getLocale()])) ? session()->get('categories')[App::getLocale()] : [];
}
// Get getProjectCategory Type
function getProjectCategory(){
    if(!isset(session()->get('ProjectCategories')[App::getLocale()])){
        $data = [];
        $categories = App\Models\Category::where('is_active',1)->where('type','project')->get();
        foreach ($categories as $category){
            $data[App::getLocale()][$category->id] = ['title'=>$category->title,'slug'=>slugfy($category->title),'tags' => $category->tags,'description' => $category->description,'type'=>$category->type];
        }
        session()->put('ProjectCategories',$data);
        session()->save();
    }
    return (isset(session()->get('ProjectCategories')[App::getLocale()])) ? session()->get('ProjectCategories')[App::getLocale()] : [];
}
// Get getBlogCategory Type
function getBlogCategory(){
    if(!isset(session()->get('BlogCategories')[App::getLocale()])){
        $data = [];
        $categories = App\Models\Category::where('is_active',1)->where('type','blog')->get();
        foreach ($categories as $category){
            $data[App::getLocale()][$category->id] = ['title'=>$category->title,'slug'=>slugfy($category->title),'tags' => $category->tags,'description' => $category->description,'type'=>$category->type];
        }
        session()->put('BlogCategories',$data);
        session()->save();
    }
    return (isset(session()->get('BlogCategories')[App::getLocale()])) ? session()->get('BlogCategories')[App::getLocale()] : [];
}
// Get Project Type
function getSpecificFeatures(){
    if(!isset(session()->get('specificFeatures')[App::getLocale()])){
        $data = [];
        $specificFeatures = App\Models\SpecificFeatures::where('is_active',1)->get();
        foreach($specificFeatures as $specificFeature){
            $data[App::getLocale()][$specificFeature->id] = [
                'title'=>$specificFeature->title,
                'slug'=>slugfy($specificFeature->title),
                'tags' => $specificFeature->tags,
                'description' => $specificFeature->meta_description
            ];
        }
        session()->put('specificFeatures',$data);
        session()->save();
    }
    return (isset(session()->get('specificFeatures')[App::getLocale()])) ? session()->get('specificFeatures')[App::getLocale()] : [];
}
function slugfy($string = null, $separator = "-")
{
    if (is_null($string)) {
        return "";
    }

    // Remove spaces from the beginning and from the end of the string
    $string = trim($string);

    // Lower case everything
    // using mb_strtolower() function is important for non-Latin UTF-8 string | more info: http://goo.gl/QL2tzK
    $string = mb_strtolower($string, "UTF-8");;

    // Make alphanumeric (removes all other characters)
    // this makes the string safe especially when used as a part of a URL
    // this keeps latin characters and arabic charactrs as well
    //$string = preg_replace("/[^a-z0-9_\s-ءاأإآؤئبتثجحخدذرزسشصضطظعغفقكلمنهويةى]/u", "", $string);

    // Remove multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);

    // Convert whitespaces and underscore to the given separator
    $string = preg_replace("/[\s_]/", $separator, $string);

    return $string;
}
//Get Project Room Count
function roomNumber(){
    return [
        '1+0',
        '1+1',
        '2+0',
        '2+1',
        '2+2',
        '3+1',
        '3+2',
        '4+1',
        '4+2',
        '4+3',
        '4+4',
        '5+1',
        '5+2',
        '5+3',
        '5+4',
        '6+1',
        '6+1',
        '6+2',
        '6+3',
        '7+1',
        '7+2',
        '7+3',
        '8+1',
        '8+2',
        '8+3',
        '8+4',
        '9+1',
        '9+2',
        '9+3',
        '9+4',
        '9+5',
        '9+6',
        '10+1',
        '10+2',
        '10+'
    ];
}
