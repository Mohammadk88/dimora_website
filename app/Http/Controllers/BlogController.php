<?php

namespace App\Http\Controllers;

use App\Repositories\BlogEloquent;
use App\Repositories\CategoryEloquent;
use App\Repositories\ProjectEloquent;
use App\Repositories\SpecificFeatureEloquent;
use App\Repositories\TagEloquent;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class BlogController extends Controller
{
    protected $categories;
    protected $projects;
    private $specificFeature;
    private $blog;
    private $tag;

    public function __construct(CategoryEloquent $categories,ProjectEloquent $projects,SpecificFeatureEloquent $specificFeature,BlogEloquent $blog,TagEloquent $tag)
    {
        $this->categories = $categories;
        $this->projects = $projects;
        $this->specificFeature = $specificFeature;
        $this->blog = $blog;
        $this->tag = $tag;

    }

    public function index()
    {
        $query['is_active'] = 1;
        $query_category['is_active'] = 1;
        $query_category['type'] = 'blog';
        $category_data = $this->categories->getWhere($query_category,['*'],false,'view','desc');
        $data['category'] = $category_data['data'];
        $data['categories'] = $category_data['data'];

        $data['current_breadcrumb'] = ['title'=>__('main.blog')];
        $data['breadcrumbs'] = [
            ['title'=>__('main.home'),'url'=>LaravelLocalization::localizeURL('/')],
        ];

        $query['publish'] = "allow";
        $query_Popular_Posts['publish'] = "allow";
        $query_Popular_Posts['is_active'] = 1;
        $query_tags['is_active'] = 1;
        $query_category['is_active'] = 1;
        $query_category['type'] = 'blog';
        $data['tags'] = [];
        $data['blogs'] = [];
        $data['Popular_Posts'] = [];
        $blogs = $this->blog->getWhereWithPages($query,6);
        $tags = $this->tag->getWhere($query_tags,['*'],false,'used','desc',12);
        $Popular_Posts = $this->blog->getWhere($query_Popular_Posts,['*'],false,'view','desc',3);
        if($blogs['status']){
            $data['blogs'] = $blogs['data'];
        }
        if($tags['status']){
            $data['tags'] = $tags['data'];
        }
        if($Popular_Posts['status']){
            $data['Popular_Posts'] = $Popular_Posts['data'];
        }
        // dd( $data['blogs'][0]['category']);
        return view('blogs.all',$data);
    }
    public function search($type,$search)
    {
        $query['is_active'] = 1;
        $query_category['is_active'] = 1;
        $query_category['type'] = 'blog';
        $category_data = $this->categories->getWhere($query_category,['*'],false,'view','desc');
        $data['category'] = $category_data['data'];
        $data['categories'] = $category_data['data'];

        $data['current_breadcrumb'] = ['title'=>__('main.blog')];
        $data['breadcrumbs'] = [
            ['title'=>__('main.home'),'url'=>LaravelLocalization::localizeURL('/')],
        ];

        $query['publish'] = "allow";
        $query_Popular_Posts['publish'] = "allow";
        $query_Popular_Posts['is_active'] = 1;
        $query_tags['is_active'] = 1;
        $query_category['is_active'] = 1;
        $query_category['type'] = 'blog';
        $data['tags'] = [];
        $data['blogs'] = [];
        $data['Popular_Posts'] = [];
        $blogs = $this->blog->getWhereWithPagesSearch($query,true,$search,6);
        $tags = $this->tag->getWhere($query_tags,['*'],false,'used','desc',12);
        $Popular_Posts = $this->blog->getWhere($query_Popular_Posts,['*'],false,'view','desc',3);
        if($blogs['status']){
            $data['blogs'] = $blogs['data'];
        }
        if($tags['status']){
            $data['tags'] = $tags['data'];
        }
        if($Popular_Posts['status']){
            $data['Popular_Posts'] = $Popular_Posts['data'];
        }
        // dd( $data['blogs'][0]['category']);
        return view('blogs.all',$data);
    }

    public function single($type,$category,$category_id,$id){
        $query['category_id'] = $category_id;
        $query['is_active'] = 1;
        $category_data = $this->categories->getOne($category_id);
        $data['category'] = $category_data['data'];
        $current_breadcrumb = ['title'=>$category_data['data']['title'],'url'=>LaravelLocalization::localizeURL('b/'.slugfy(__('main.blog')).'/'.$category.'/'.$category_id)];
        $data['current_breadcrumb'] = $current_breadcrumb;
        $data['breadcrumbs'] = [
            ['title'=>__('main.home'),'url'=>LaravelLocalization::localizeURL('/')],
            ['title'=>__('main.blog'),'url'=>LaravelLocalization::localizeURL('b/'.slugfy(__('main.blog')))],
            ['title'=>$category_data['data']['title'],'url'=>LaravelLocalization::localizeURL('b/'.slugfy(__('main.blog')).'/'.$category.'/'.$category_id)]
        ];

        $query['publish'] = "allow";
        $query_Popular_Posts['publish'] = "allow";
        $query_Popular_Posts['is_active'] = 1;
        $query_tags['is_active'] = 1;
        $query_category['is_active'] = 1;
        $query_category['type'] = 'blog';
        $data['tags'] = [];
        $data['categories'] = [];
        $data['blog'] = [];
        $data['Popular_Posts'] = [];
        $blog = $this->blog->getOne($id);
        $tags = $this->tag->getWhere($query_tags,['*'],false,'used','desc',12);
        $Popular_Posts = $this->blog->getWhere($query_Popular_Posts,['*'],false,'view','desc',3);
        $categories = $this->categories->getWhere($query_category,['*'],false,'view','desc');
        if($blog['status']){
            $data['blog'] = $blog['data'];
            $current_breadcrumb = ['title'=>$data['blog']['title']];
            $data['current_breadcrumb'] = $current_breadcrumb;

        }
        if($tags['status']){
            $data['tags'] = $tags['data'];
        }
        if($Popular_Posts['status']){
            $data['Popular_Posts'] = $Popular_Posts['data'];
        }
        if($categories['status']){
            $data['categories'] = $categories['data'];
        }
        $this->blog->setView($id);
        $this->categories->setView($data['blog']['category']['id']);
        $this->categories->setPoints($data['blog']['category']['id']);

        // dd( $data['blogs'][0]['category']);
        return view('blogs.single',$data);
    }

}
