<?php

namespace App\Http\Controllers;

use App\Models\SpecificFeatures;
use App\Repositories\BlogEloquent;
use App\Repositories\CategoryEloquent;
use App\Repositories\ProjectEloquent;
use App\Repositories\SpecificFeatureEloquent;
use App\Repositories\TagEloquent;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class CategoryController extends Controller
{
    protected $categories;
    protected $projects;
    private $specificFeature;
    private $blog;
    private $tag;

    public function __construct(CategoryEloquent $categories,ProjectEloquent $projects,SpecificFeatureEloquent $specificFeature,BlogEloquent $blog,TagEloquent $tag)
    {
        $this->categories = $categories;
        $this->projects = $projects;
        $this->specificFeature = $specificFeature;
        $this->blog = $blog;
        $this->tag = $tag;

    }

    public function project($type,$category,$category_id){
        $data['category_id'] = $category_id;
        $query['is_active'] = 1;
        $category_data = $this->categories->getOne($category_id);
        $data['category'] = $category_data['data'];
        $current_breadcrumb = ['title'=>$category_data['data']['title'],'url'=>LaravelLocalization::localizeURL('p/'.slugfy(__('main.projects')).'/'.$category.'/'.$category_id)];
        $data['current_breadcrumb'] = $current_breadcrumb;
        $data['breadcrumbs'] = [
            ['title'=>__('main.home'),'url'=>LaravelLocalization::localizeURL('/')],
            ['title'=>__('main.projects'),'url'=>LaravelLocalization::localizeURL('p/'.slugfy(__('main.projects')))]
        ];
        $data['specificFeatures'] = [];
        $specificFeature = $this->specificFeature->getWhere($query,['*'],false,'order','asc',7);
        if($specificFeature['status']){
            $data['specificFeatures'] = $specificFeature['data'];
        }
        $query['publish'] = "allow";
        $data['projects'] = [];
        $projects = $this->projects->getWhereWithPages($query,9);
        if($projects['status']){
            $data['projects'] = $projects['data'];
        }
        $this->categories->setView($category_id);
        return view('projects.categories',$data);
    }

    public function specificFeatures($type,$specificFeatures,$specificFeatures_id){
        $data['projects'] = [];
        $query['is_active']=1;
        $specificFeature = SpecificFeatures::find($specificFeatures_id);
        $data['category'] = $specificFeature;
        $projects_ids = $specificFeature->getProjectIdsAttribute();
        $projects = $this->projects->getWhereInWithPages('id',[$projects_ids],9);
        if ($projects['status']){
            $data['projects'] = $projects['data'];
        }
        $data['specificFeatures'] = [];
        $specificFeatures_all = $this->specificFeature->getWhere($query,['*'],false,'order','asc',7);
        if($specificFeatures_all['status']){
            $data['specificFeatures'] = $specificFeatures_all['data'];
        }
        $current_breadcrumb = ['title'=>$specificFeature['title'],'url'=>LaravelLocalization::localizeURL('/').'/p/'.__('main.projects').'/s/'.slugfy($specificFeature['title'])];
        $data['current_breadcrumb'] = $current_breadcrumb;
        $data['breadcrumbs'] = [
            ['title'=>__('main.home'),'url'=>LaravelLocalization::localizeURL('/')],
            ['title'=>__('main.projects'),'url'=>LaravelLocalization::localizeURL('p/'.slugfy(__('main.projects')))]
        ];
        return view('projects.categories',$data);
    }

    public function blog($type,$category,$category_id){
        $query['category_id'] = $category_id;
        $query['is_active'] = 1;
        $category_data = $this->categories->getOne($category_id);
        $data['category'] = $category_data['data'];
        $current_breadcrumb = ['title'=>$category_data['data']['title'],'url'=>LaravelLocalization::localizeURL('b/'.slugfy(__('main.blog')).'/'.$category.'/'.$category_id)];
        $data['current_breadcrumb'] = $current_breadcrumb;
        $data['breadcrumbs'] = [
            ['title'=>__('main.home'),'url'=>LaravelLocalization::localizeURL('/')],
            ['title'=>__('main.blog'),'url'=>LaravelLocalization::localizeURL('b/'.slugfy(__('main.blog')))]
        ];

        $query['publish'] = "allow";
        $query_Popular_Posts['publish'] = "allow";
        $query_Popular_Posts['is_active'] = 1;
        $query_tags['is_active'] = 1;
        $query_category['is_active'] = 1;
        $query_category['type'] = 'blog';
        $data['tags'] = [];
        $data['categories'] = [];
        $data['blogs'] = [];
        $data['Popular_Posts'] = [];
        $blogs = $this->blog->getWhereWithPages($query,6);
        $tags = $this->tag->getWhere($query_tags,['*'],false,'used','desc',12);
        $Popular_Posts = $this->blog->getWhere($query_Popular_Posts,['*'],false,'view','desc',3);
        $categories = $this->categories->getWhere($query_category,['*'],false,'view','desc');
        if($blogs['status']){
            $data['blogs'] = $blogs['data'];
        }
        if($tags['status']){
            $data['tags'] = $tags['data'];
        }
        if($Popular_Posts['status']){
            $data['Popular_Posts'] = $Popular_Posts['data'];
        }
        if($categories['status']){
            $data['categories'] = $categories['data'];
        }
        $this->categories->setView($category_id);

        // dd( $data['blogs'][0]['category']);
        return view('blogs.categories',$data);
    }
}
