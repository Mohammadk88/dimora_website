<?php

namespace App\Http\Controllers;

use App\Repositories\FaqEloquent;
use App\Repositories\TestimonialEloquent;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class PagesController extends Controller
{
    private $testimonial;
    private $faq;
    public function __construct(TestimonialEloquent $testimonial,FaqEloquent $faq)
    {
        $this->testimonial = $testimonial;
        $this->faq = $faq;
    }
    public function aboutUs(){
        $data['current_breadcrumb'] = ['title'=>__('main.links.aboutUs')];
        $data['breadcrumbs'] = [
            ['title'=>__('main.home'),'url'=>LaravelLocalization::localizeURL('/')],
        ];
        $testimonial = $this->testimonial->getAll();
        $data['testimonials'] = [];
        if($testimonial['status']){
            $data['testimonials'] = $testimonial['data'];
        }
        return view('pages.about_us',$data);
    }
    public function contactUs(){
        $data['current_breadcrumb'] = ['title'=>__('main.ContactUs')];
        $data['breadcrumbs'] = [
            ['title'=>__('main.home'),'url'=>LaravelLocalization::localizeURL('/')],
        ];

        return view('pages.contact_us',$data);
    }
    public function faq(){
        $data['current_breadcrumb'] = ['title'=>__('main.help_center')];
        $data['breadcrumbs'] = [
            ['title'=>__('main.home'),'url'=>LaravelLocalization::localizeURL('/')],
        ];
        $faqs = $this->faq->getAll();
        $data['faqs'] = [];
        if($faqs['status']){
            $data['faqs'] = $faqs['data'];
        }
        return view('pages.faq',$data);
    }
    public function citizenship(){
        $data['current_breadcrumb'] = ['title'=>__('main.links.citizenship')];
        $data['breadcrumbs'] = [
            ['title'=>__('main.home'),'url'=>LaravelLocalization::localizeURL('/')],
        ];

        return view('pages.citizenship',$data);
    }
}
