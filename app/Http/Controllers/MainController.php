<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\BlogEloquent;
use App\Repositories\ProjectEloquent;
use App\Repositories\SlideEloquent;
use App\Repositories\SpecificFeatureEloquent;
use App\Repositories\TestimonialEloquent;

class MainController extends Controller
{
    private $slide;
    private $specificFeature;
    private $projects;
    private $blog;
    private $testimonial;
    public function __construct(SlideEloquent $slide,SpecificFeatureEloquent $specificFeature,ProjectEloquent $projects,BlogEloquent $blog,TestimonialEloquent $testimonial)
    {
        $this->slide = $slide;
        $this->specificFeature = $specificFeature;
        $this->projects = $projects;
        $this->blog = $blog;
        $this->testimonial = $testimonial;
    }
    public function sessionFlash(){
        session()->flush();
    }
    public function index(){
       // dd(getCategory());
        //dd(url()->current());
        //dd(session('currencies'));
        $query['is_active']=1;
        $slide = $this->slide->getWhere($query,['*'],false,'order','asc',4);
        $data['slides'] = [];
        if($slide['status']){
            $data['slides'] = $slide['data'];
        }
        $data['specificFeatures'] = [];
        $specificFeature = $this->specificFeature->getWhere($query,['*'],false,'order','asc',7);
        if($specificFeature['status']){
            $data['specificFeatures'] = $specificFeature['data'];
        }
        //return dd($data['specificFeatures'][1]['photo_alt']);

        $data['projects'] = [];
        $queryProject['is_active'] = 1;
        $queryProject['publish'] = "allow";
        $projects = $this->projects->getWhere($queryProject,['*'],false,'view','desc',10);
        if($projects['status']){
            $data['projects'] = $projects['data'];
        }
        $data['project_specials'] = [];
        $queryProjectSpecial = $queryProject;
        $queryProjectSpecial['is_special'] = 1;
        $projectSpecials = $this->projects->getWhere($queryProjectSpecial,['*'],false,'points','desc',3);
        if($projectSpecials['status']){
            $data['projectSpecials'] = $projectSpecials['data'];
        }
        $data['blogs'] = [];
        $queryBlog['is_active'] = 1;
        $queryBlog['publish'] = "allow";
        $blogs = $this->blog->getWhere($queryBlog,['*'],false,'view','desc',3);
        if($blogs['status']){
            $data['blogs'] = $blogs['data'];
        }
        $testimonial = $this->testimonial->getAll();
        $data['testimonials'] = [];
        if($testimonial['status']){
            $data['testimonials'] = $testimonial['data'];
        }
       // dd($testimonial['data']->toArray());
        return view('index',$data);
    }
}
