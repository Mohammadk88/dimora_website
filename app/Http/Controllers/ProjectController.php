<?php

namespace App\Http\Controllers;

use App\Repositories\CategoryEloquent;
use App\Repositories\ProjectEloquent;
use App\Repositories\SpecificFeatureEloquent;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ProjectController extends Controller
{
    protected $categories;
    protected $projects;
    protected $specificFeature;

    public function __construct(CategoryEloquent $categories,ProjectEloquent $projects,SpecificFeatureEloquent $specificFeature)
    {
        $this->categories = $categories;
        $this->projects = $projects;
        $this->specificFeature = $specificFeature;

    }
    public function index()
    {
        $query['is_active'] = 1;
        $data['current_breadcrumb'] = ['title'=>__('main.projects')];
        $data['breadcrumbs'] = [
            ['title'=>__('main.home'),'url'=>LaravelLocalization::localizeURL('/')],
        ];
        $data['specificFeatures'] = [];
        $specificFeature = $this->specificFeature->getWhere($query,['*'],false,'order','asc',7);
        if($specificFeature['status']){
            $data['specificFeatures'] = $specificFeature['data'];
        }
        $query['publish'] = "allow";
        $data['projects'] = [];
        $projects = $this->projects->getWhereWithPages($query,9);
        if($projects['status']){
            $data['projects'] = $projects['data'];
        }
        return view('projects.all',$data);
    }
    public  function single($type,$category,$category_id,$project_id){
        $data['category_id'] = $category_id;
        $query['is_active'] = 1;
        $category_data = $this->categories->getOne($category_id);
        $data['category'] = $category_data['data'];
        $current_breadcrumb = ['title' => $category_data['data']['title']];
        $data['breadcrumbs'] = [
            ['title'=>__('main.home'),'url'=>LaravelLocalization::localizeURL('/')],
            ['title'=>__('main.projects'),'url'=>LaravelLocalization::localizeURL('p/'.slugfy(__('main.projects')))],
            ['title'=>$category_data['data']['title'],'url'=>LaravelLocalization::localizeURL('p/'.slugfy(__('main.projects')).'/'.$category.'/'.$category_id)]
        ];
        $data['project'] = [];
        $project = $this->projects->getOne($project_id);
        if($project['status']){
            $data['project'] = $project['data'];
            $current_breadcrumb = ['title'=>$data['project']['title']];
            $data['current_breadcrumb'] = $current_breadcrumb;

        }
        return view('projects.single',$data);
    }
}
