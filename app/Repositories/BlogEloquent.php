<?php

namespace App\Repositories;

use App\Models\Blog;
use App\Repositories\MainEloquent;

class BlogEloquent extends MainEloquent{

    /**
     * @return mixed
     */
    public function model()
    {
        return Blog::class;
    }
}
