<?php

namespace App\Repositories;

use Exception;
use Illuminate\Http\Request;
use PDOException;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;

abstract class MainEloquent {

    protected $model;
    abstract public function model();
    /**
     * MainEloquent constructor.
     */
     public function __construct()
     {
         $this->model = app()->make($this->model());
     }

    /**
     * @param Request $request
     * @return array
     */
    public function inputData(Request $request): array {
        $data = [];
        foreach ($request->all() as $index => $value){
            $data[$index] = $request->input($index);
        }
        return $data;
    }
    /**
     * @param array $array
     * @return mixed
     * @throws Exception
     */
     public function create(array $array): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $array['uuid'] = Uuid::uuid4();
             $data = $this->model->create($array);
             $status = true;
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             if (($ex->getCode() == 1062 || $ex->getCode() == 23000)) {
                 $error[] = 'Duplicated';
                 $requestCode = $ex->getCode();
             }
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }


    /**
      * @param $id
      * @param array $array
      * @return mixed
      */
     public function update($id, array $array): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $data = $this->model->find($id)->update($array);
             $status = true;
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             if (($ex->getCode() == 1062 || $ex->getCode() == 23000)) {
                 $error[] = 'Duplicated';
             }
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

     /**
      * @param $id
      * @return mixed
      */
     public function destroy(array $id): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $this->model->whereIn('id', $id)->update($array);
             $this->model->whereIn('id', $id)->delete();
             $status = true;
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

     /**
      * @param $id
      * @return mixed
      */
     public function delete(array $id): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $this->model->whereIn('id', $id)->forceDelete();
             $status = true;
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

    /**
     * @param array $array
     * @param bool $withTrash
     * @return mixed
     */
     public function count(array $array = [],$withTrash = false): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $data = $this->model;
             if(count($array) != 0){
                 $data = $data->where($array);
             }
             if($withTrash){
                 $data = $data->withTrashed();
             }
             $data = $data->count();
             $status = true;
         } catch (PDOException $ex) {
              $error[] = $ex->getMessage();
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

    /**
     * @param bool $withTrash
     * @param array $array
     * @param array $fields
     * @param string $order
     * @param string $orderType
     * @return mixed
     */
    public function getAll($withTrash = false,array $array = null, array $fields = ['*'],$order="id",$orderType="desc"): array
    {
        $data = null;
        $error = null;
        $status = false;
        $requestCode = 200;
        try {
            $data = $this->model;
            if($withTrash){
                $data = $data->withTrashed();
            }
            if($array != null){
                $data = $data->where($array);
            }
            $data = $data->orderBy($order,$orderType);
            $data = $data->get($fields);
            $status = true;
        } catch (PDOException $ex) {
            $error[] = $ex->getMessage();
            $requestCode = $ex->getCode();
        }
        $result = [
            'status' => $status,
            'data' => $data,
            'error' => $error,
            'code' => $requestCode
        ];
        return $result;
    }

    /**
     * @param bool $city
     * @param bool $region
     * @param bool $neighborhood
     * @return mixed
     */
    public function getAllLocation(bool $city = false,bool $region = false, bool $neighborhood = false): array
    {
        $data = null;
        $error = null;
        $status = false;
        $requestCode = 200;
        try {
            if($city){
                $data['city'] =  DB::table('sehir')->get();
            }
            if($region){
                $data['region'] =  DB::table('ilce')->get();
            }
            if($neighborhood){
                $data['neighborhood'] =  DB::table('mahalle')->get();
            }
            $status = true;
        } catch (PDOException $ex) {
            $error[] = $ex->getMessage();
            $requestCode = $ex->getCode();
        }
        $result = [
            'status' => $status,
            'data' => $data,
            'error' => $error,
            'code' => $requestCode
        ];
        return $result;
    }

    /**
     * @param $with
     * @param bool $withTrash
     * @param array $array
     * @param array $fields
     * @param string $order
     * @param string $orderType
     * @return mixed
     */
     public function getWith($with, $withTrash = false,array $array = [], array $fields = ['*'],$order="id",$orderType="desc"): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $data = $this->model->with($with);
             if($withTrash){
                 $data = $data->withTrashed();
             }
             if(count($array) != 0){
                 $data = $data->where($array);
             }
             $data = $data->orderBy($order,$orderType);

             $data = $data->get($fields);
             $status = true;
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

    /**
     * @param $by
     * @param $value
     * @param bool $withTrash
     * @param string $order
     * @param string $orderType
     * @return mixed
     */
     public function getBy($by, $value,$withTrash = false,$order="id",$orderType="desc"): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $data = $this->model;
             if($withTrash){
                 $data = $data->withTrashed();
             }
             $data = $data->where($by, $value)->orderBy($order,$orderType)->get();
             $status = true;
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

    /**
     * @param $id
     * @return mixed
     */
    public function getOne($id): array
    {
        $data = null;
        $error = null;
        $status = false;
        $requestCode = 200;
        try {
            $data = $this->model->find($id);
            $status = true;
        } catch (PDOException $ex) {
            $error[] = $ex->getMessage();
            $requestCode = $ex->getCode();
        }
        return [
            'status' => $status,
            'data' => $data,
            'error' => $error,
            'code' => $requestCode
        ];
    }

    /**
     * @param array $array
     * @param array $fields
     * @param bool $withTrash
     * @param string $order
     * @param string $orderType
     * @param int $limit
     * @return mixed
     */
     public function getWhere(array $array = [], array $fields = ['*'],$withTrash = false,$order="id",$orderType="desc",$limit = 0): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $data = $this->model;
             if($withTrash){
                 $data = $data->withTrashed();
             }
             if(count($array) != 0){
                 $data = $data->where($array);
             }
             if($limit != 0){
                 $data = $data->limit($limit);
             }
             $data = $data->orderBy($order,$orderType)->get($fields);
             $status = true;
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

    /**
     * @param array $array
     * @param String $search
     * @param $perPage
     * @param bool $withTrash
     * @param string $order
     * @param string $orderType
     * @param int $limit
     * @return mixed
     */
    public function getWhereWithPagesSearch(array $array = [],$searchAble = false,String $search,$perPage ,$withTrash = false,$order="id",$orderType="desc",$limit = 0): array
    {
        $data = null;
        $error = null;
        $status = false;
        $requestCode = 200;
        try {
            $data = $this->model;
            if($withTrash){
                $data = $data->withTrashed();
            }
            if(count($array) != 0){
                $data = $data->where($array);
            }
            if($limit != 0){
                $data = $data->limit($limit);
            }
            if($searchAble){
                $data = $data->whereTranslationLike('title','%'.$search.'%');
                $data = $data->orWhereTranslationLike('description','%'.$search.'%');
               $data = $data->orWhereTranslationLike('meta_description','%'.$search.'%');
            }
            $data = $data->orderBy($order,$orderType)->paginate($perPage);
            $status = true;
        } catch (PDOException $ex) {
            $error[] = $ex->getMessage();
            $requestCode = $ex->getCode();
        }
        $result = [
            'status' => $status,
            'data' => $data,
            'error' => $error,
            'code' => $requestCode
        ];
        return $result;
    }
    /**
     * @param array $array
     * @param $perPage
     * @param bool $withTrash
     * @param string $order
     * @param string $orderType
     * @param int $limit
     * @return mixed
     */
    public function getWhereWithPages(array $array = [],$perPage ,$withTrash = false,$order="id",$orderType="desc",$limit = 0): array
    {
        $data = null;
        $error = null;
        $status = false;
        $requestCode = 200;
        try {
            $data = $this->model;
            if($withTrash){
                $data = $data->withTrashed();
            }
            if(count($array) != 0){
                $data = $data->where($array);
            }
            if($limit != 0){
                $data = $data->limit($limit);
            }
            $data = $data->orderBy($order,$orderType)->paginate($perPage);
            $status = true;
        } catch (PDOException $ex) {
            $error[] = $ex->getMessage();
            $requestCode = $ex->getCode();
        }
        $result = [
            'status' => $status,
            'data' => $data,
            'error' => $error,
            'code' => $requestCode
        ];
        return $result;
    }

    /**
     * @param string $field
     * @param array $values
     * @param $perPage
     * @param bool $withTrash
     * @param string $order
     * @param string $orderType
     * @return mixed
     */
    public function getWhereInWithPages(string $field,array $values,$perPage ,$withTrash = false,$order="id",$orderType="desc"): array
    {
        $data = null;
        $error = null;
        $status = false;
        $requestCode = 200;
        try {
            $data = $this->model;
            if($withTrash){
                $data = $data->withTrashed();
            }
            $data = $data->whereIn($field, $values)->orderBy($order,$orderType)->paginate($perPage);
            $status = true;
        } catch (PDOException $ex) {
            $error[] = $ex->getMessage();
            $requestCode = $ex->getCode();
        }
        $result = [
            'status' => $status,
            'data' => $data,
            'error' => $error,
            'code' => $requestCode
        ];
        return $result;
    }

    /**
     * @param $by
     * @param $value
     * @param string $order
     * @param string $orderType
     * @return mixed
     */
     public function firstBy($by, $value,$order="id",$orderType="desc"): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $data = $this->model->orderBy($order,$orderType)->where($by, $value)->first();
             $status = true;
             if (!$data) {
                 $data = null;
                 $error[] = 'not_found';
                 $status = false;
                 $requestCode = 404;
             }
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

    /**
     * @param array $array
     * @param string $order
     * @param string $orderType
     * @return mixed
     */
     public function firstWhere(array $array,$order="id",$orderType="desc"): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             if(count($array) != 0){
                 $data = $this->model->orderBy($order,$orderType)->where($array)->first();
                 $status = true;
                 if (!$data) {
                     $data = null;
                     $error[] = 'not_found';
                     $status = false;
                     $requestCode = 404;
                 }
             }
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

     /**
      * @param $with
      * @param array $array
      * @return mixed
      */
     public function firstWith($with, array $array = [],$order="id",$orderType="desc"): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $data = $this->model->orderBy($order,$orderType)->with($with)->where($array)->first();
             $status = true;
             if (!$data) {
                 $data = null;
                 $error[] = 'not_found';
                 $status = false;
                 $requestCode = 404;
             }
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

     /**
      * @param array $array
      * @return mixed
      */
     public function trashCount(array $array = []): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $data = $this->model->onlyTrashed();
             if(count($array) != 0){
                 $data =$data->where($array);
             }
             $data = $data->count();
             $status = true;
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

     /**
      * @param $with
      * @param array $array
      * @param array $fields
      * @return mixed
      */
     public function trashGetWith($with, array $array = [], array $fields = ['*']): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $data = $this->model->onlyTrashed()->with($with);
             if(count($array) != 0){
                 $data =$data->where($array);
             }
             $data = $data->get($fields);
             $status = true;
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

     /**
      * @param $by
      * @param $value
      * @return mixed
      */
     public function trashGetBy($by, $value): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $data = $this->model->onlyTrashed()->where($by, $value)->get();
             $status = true;
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

     /**
      * @param array $array
      * @param array $fields
      * @return mixed
      */
     public function trashGetWhere(array $array, array $fields = ['*']): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $data = $this->model->onlyTrashed();
             if(count($array) != 0){
                 $data =$data->where($array);
             }
             $data = $data->get($fields);
             $status = true;
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

     /**
      * @param $by
      * @param $value
      * @return mixed
      */
     public function trashFirstBy($by, $value): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $data = $this->model->onlyTrashed()->where($by, $value)->first();
             $status = true;
             if (!$data) {
                 $data = null;
                 $error[] = 'not_found';
                 $status = false;
             }
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

     /**
      * @param array $array
      * @return mixed
      */
     public function trashFirstWhere(array $array): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $data = $this->model->onlyTrashed()->where($array)->first();
             $status = true;
             if (!$data) {
                 $data = null;
                 $error[] = 'not_found';
                 $status = false;
                 $requestCode = 404;
             }
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

     /**
      * @param $with
      * @param array $array
      * @return mixed
      */
     public function trashFirstWith($with, array $array = []): array
     {
         $data = null;
         $error = null;
         $status = false;
         $requestCode = 200;
         try {
             $data = $this->model->onlyTrashed()->with($with)->where($array)->first();
             $status = true;
             if (!$data) {
                 $data = null;
                 $error[] = 'not_found';
                 $status = false;
                 $requestCode = 404;
             }
         } catch (PDOException $ex) {
             $error[] = $ex->getMessage();
             $requestCode = $ex->getCode();
         }
         $result = [
             'status' => $status,
             'data' => $data,
             'error' => $error,
             'code' => $requestCode
         ];
         return $result;
     }

    /**
     * @param $id
     * @return mixed
     */
    public function active($id): array
    {
      return  $this->update($id,['is_active'=> 1]);
    }

    /**
     * @param $id
     * @return array
     */
    public function inActive($id):array
    {
        return  $this->update($id,['is_active'=> 0]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function block($id): array
    {
        return  $this->update($id,['is_blocked' => 1]);
    }

    /**
     * @param $id
     * @return array
     */
    public function allow($id): array
    {
        return  $this->update($id,['is_blocked' => 0]);
    }
    /**
     * @param $id
     * @return array
     */
    public function setView($id): array
    {
        $data = $this->getOne($id);
        $view = 0;
        if($data['status']){
            $view = $data['data']['view'];
        }
        return  $this->update($id,['view' => $view+1]);
    }
    /**
     * @param $id
     * @return array
     */
    public function setPoints($id): array
    {
        $data = $this->getOne($id);
        $points = 0;
        if($data['status']){
            $points = $data['data']['points'];
        }
        return  $this->update($id,['points' => $points+1]);
    }
}
