<?php

namespace App\Repositories;

use App\Models\Slider;
use App\Repositories\MainEloquent;

class SlideEloquent extends MainEloquent{

    /**
     * @return mixed
     */
    public function model()
    {
        return Slider::class;
    }
}
