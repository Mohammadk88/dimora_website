<?php

namespace App\Repositories;

use App\Models\Category;
use App\Repositories\MainEloquent;

class CategoryEloquent extends MainEloquent{

    /**
     * @return mixed
     */
    public function model()
    {
        return Category::class;
    }
}
