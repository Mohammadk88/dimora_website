<?php

namespace App\Repositories;

use App\Models\Tag;
use App\Repositories\MainEloquent;

class TagEloquent extends MainEloquent{

    /**
     * @return mixed
     */
    public function model()
    {
        return Tag::class;
    }
}
