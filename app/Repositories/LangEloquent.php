<?php

namespace App\Repositories;

use App\Models\Language;
use App\Repositories\MainEloquent;

class LangEloquent extends MainEloquent{

    /**
     * @return mixed
     */
    public function model()
    {
        return Language::class;
    }
}
