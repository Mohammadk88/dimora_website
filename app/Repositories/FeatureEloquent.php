<?php

namespace App\Repositories;

use App\Models\Feature;
use App\Repositories\MainEloquent;

class FeatureEloquent extends MainEloquent{

    /**
     * @return mixed
     */
    public function model()
    {
        return Feature::class;
    }
}
