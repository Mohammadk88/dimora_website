<?php

namespace App\Repositories;

use App\Models\SpecificFeatures;
use App\Repositories\MainEloquent;

class SpecificFeatureEloquent extends MainEloquent{

    /**
     * @return mixed
     */
    public function model()
    {
        return SpecificFeatures::class;
    }
}
