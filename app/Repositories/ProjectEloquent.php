<?php

namespace App\Repositories;

use App\Models\Project;
use App\Repositories\MainEloquent;

class ProjectEloquent extends MainEloquent{

    /**
     * @return mixed
     */
    public function model()
    {
        return Project::class;
    }
}
