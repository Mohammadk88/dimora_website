<?php

namespace App\Repositories;

use App\Models\Faq;
use App\Repositories\MainEloquent;

class FaqEloquent extends MainEloquent{

    /**
     * @return mixed
     */
    public function model()
    {
        return Faq::class;
    }
}
