<?php

namespace App\Repositories;

use App\Models\Tag;
use App\Models\Testimonial;
use App\Repositories\MainEloquent;

class TestimonialEloquent extends MainEloquent{

    /**
     * @return mixed
     */
    public function model()
    {
        return Testimonial::class;
    }
}
