<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Image;
use File;

trait UploadTrait
{
    /**
     * @param UploadedFile $uploadedFile
     * @param null $folder
     * @param string $disk
     * @param null $filename
     * @return false|string
     */
    public function uploadOne(UploadedFile $uploadedFile, $folder = null, $disk = 'public', $filename = null)
    {
        $name = !is_null($filename) ? $filename : Str::random(25);

        return $uploadedFile->storeAs($folder, $name.'.'.$uploadedFile->getClientOriginalExtension(), $disk);
    }

    public static function ImagesUpload(UploadedFile $uploadedFile, $folder,$thumb = 0,$filename = null, $mode = 0777){
        try {
            $file = $filename;
            if($filename ==null){
                $file = time().rand().'.'.$uploadedFile->getClientOriginalExtension();
            }
            $path_orginal = public_path('uploads\\'.$folder.'\\');
            $path_thumbnail = public_path('uploads\\'.$folder.'\thumbnail\\');
            $img = Image::make($uploadedFile);
          //  Storage::makeDirectory($path_orginal);
            File::makeDirectory($path_orginal, $mode, true, true);

            $img->save($path_orginal.$file);
           // $uploadedFile->storeAs($path_orginal, $file, 'local');
            if($thumb != 0){
               // Storage::makeDirectory($path_thumbnail);
                File::makeDirectory($path_thumbnail, $mode, true, true);
                $thamp = Image::make($uploadedFile);
                $thamp->resize(409, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                $thamp->save($path_thumbnail.$file);
            }
            return ['status'=>true,'file_name' => $file];
        }catch (\Exception $ex){
            return ['status' => false, 'error' => $ex->getMessage()];
        }
    }

    public static function ImagesDelete($photo, $folder){
        if (Storage::exists(storage_path('uploads\\'.$folder.'\thumbnail\\'.$photo))) {
            Storage::delete(storage_path('uploads\\'.$folder.'\thumbnail\\'.$photo));
        }
        if (Storage::exists(storage_path('uploads\\'.$folder.'\\'.$photo))) {
            Storage::delete(storage_path('uploads\\'.$folder.'\\'.$photo));
        }
        return 1;
    }
}
