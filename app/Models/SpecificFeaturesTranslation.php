<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpecificFeaturesTranslation extends Model
{
    public $timestamps = false;
    protected $table="specific_feature_translations";
    protected $fillable = ['title','photo_alt','keyword','meta_description','description','locale','tags'];
}
