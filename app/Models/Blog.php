<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model implements TranslatableContract
{
    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = ['title','photo_alt','meta_keyword','meta_description','description','locale','tags'];
    protected $fillable = ['id','category_id','photo','slug','key','uuid','publish','view','points','is_active','is_blocked','created_user','updated_user','deleted_user','created_at','updated_at','deleted_at'];
    protected $relations =['translation','category'];

    /**
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

}
