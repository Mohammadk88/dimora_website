<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpecificFeatures extends Model implements TranslatableContract
{
    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = ['title','photo_alt','keyword','meta_description','description','locale','tags'];
    protected $fillable = ['id','slug','uuid','view','points','is_active','created_user','updated_user','deleted_user','created_at','updated_at','deleted_at'];
    protected $relations =['translation','projects'];

    /**
     * @return BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany('App\Models\Project','specific_feature_project')->where('is_active',1)->where('publish','allow');
    }

    public function getProjectIdsAttribute()
    {
        return $this->projects->pluck('id');
    }
}
