<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends Model implements TranslatableContract
{
    use Translatable;
    use SoftDeletes;
    public $translatedAttributes = ['question','answer','locale'];
    protected $fillable = ['id','created_user','updated_user','deleted_user','created_at','updated_at','deleted_at'];
    protected $relations =['translation'];

}
