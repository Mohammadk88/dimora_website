<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BlogTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title','photo_alt','meta_keyword','meta_description','description','locale','tags'];

}
