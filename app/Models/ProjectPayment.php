<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ProjectPayment extends Model implements TranslatableContract
{
    use Translatable;
    public $translatedAttributes = ['description','locale'];
    protected $fillable = ['id','currency','type','project_id','price','duration','duration_type','view','points','is_active','created_user','updated_user','deleted_user','created_at','updated_at','deleted_at'];
    protected $relations =['translation','projects'];

    public function projects()
    {
        return $this->belongsTo('App\Models\Project');
    }
}
