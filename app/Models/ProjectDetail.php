<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ProjectDetail extends Model implements TranslatableContract
{
    use Translatable;
    public $translatedAttributes = ['title','description','locale'];
    protected $fillable = ['id','key','open','project_id','order','is_active','created_user','updated_user','deleted_user','created_at','updated_at','deleted_at'];
    protected $relations =['translation','projects'];

    public function projects()
    {
        return $this->belongsTo('App\Models\Project');
    }
}
