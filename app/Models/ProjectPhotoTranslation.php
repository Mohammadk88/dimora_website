<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectPhotoTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['photo_alt','locale'];
}
