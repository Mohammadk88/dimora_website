<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Testimonial extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['name','position','text','locale'];
    protected $fillable = ['id','review','avatar','via','created_user','updated_user','deleted_user','created_at','updated_at'];
    protected $relations =['translation'];
}
