<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model
{
    use SoftDeletes;

    protected $fillable = ['id','photo','order','key','uuid','tags','view','points','is_active','created_user','updated_user','deleted_user','created_at','updated_at','deleted_at'];

}
