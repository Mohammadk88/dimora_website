<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = ['id','key','uuid','icon','code','default','is_active','created_user','updated_user','deleted_user','created_at','updated_at','deleted_at'];

}
