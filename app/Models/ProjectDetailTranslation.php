<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectDetailTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title','description','locale'];
}
