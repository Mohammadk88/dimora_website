<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model implements TranslatableContract
{
    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = ['title','locale','tags','description'];
    protected $fillable = ['id','slug','uuid','type','icons','parent','view','points','is_active','created_user','updated_user','deleted_user','created_at','updated_at','deleted_at'];
    protected $relations =['translation','projects'];

    /**
     * @return HasMany
     */
    public function blogs()
    {
        return $this->hasMany('App\Models\Blog');
    }
    /**
     * @return HasMany
     */
    public function projects()
    {
        return $this->hasMany('App\Models\Project');
    }
}
