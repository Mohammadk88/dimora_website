<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectPaymentTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['description','locale'];
}
