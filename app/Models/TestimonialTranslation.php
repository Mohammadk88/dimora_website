<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TestimonialTranslation extends Model
{
    protected $table = "testimonials_translations";
    public $timestamps = false;
    protected $fillable = ['name','position','text','locale'];

}
