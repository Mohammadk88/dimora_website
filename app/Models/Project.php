<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model implements TranslatableContract
{
    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = ['title','description','tags','meta_keyword','meta_description','locale'];
    protected $fillable = ['id','category_id','ref_id','slug','key','uuid','cover','phone','location','room_number','price','city','region','delivery_date','status','location-map','neighborhood','area','email','website','similar','publish','is_special','view','points','is_active','is_blocked','created_user','updated_user','deleted_user','created_at','updated_at','deleted_at'];
    protected $relations =['translation','types','Category','photos','payments','features','details','specific_feature'];

    /**
     * @return BelongsTo
     */
    public function Category()
    {
        return $this->belongsTo('App\Models\Category');
    }
    /**
     * @return HasMany
     */
    public function photos()
    {
        return $this->hasMany('App\Models\ProjectPhoto');
    }
    /**
     * @return HasMany
     */
    public function payments()
    {
        return $this->hasMany('App\Models\ProjectPayment');
    }

    /**
     * @return BelongsToMany
     */
    public function features()
    {
        return $this->belongsToMany('App\Models\Feature','project_features');
    }
    /**
     * @return HasMany
     */
    public function details()
    {
        return $this->hasMany('App\Models\ProjectDetail')->orderBy('order','asc');
    }/**
     * @return HasMany
     */
    public function types()
    {
        return $this->hasMany('App\Models\ProjectType');
    }
    public function specificFeature(){
        return $this->belongsToMany('App\Models\SpecificFeatures','specific_feature_project');
    }
}
