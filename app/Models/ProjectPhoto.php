<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ProjectPhoto extends Model implements TranslatableContract
{
    use Translatable;
    public $translatedAttributes = ['photo_alt','locale'];
    protected $fillable = ['id','file','type','project_id','view','points','is_active','created_user','updated_user','deleted_user','created_at','updated_at','deleted_at'];
    protected $relations =['translation','projects'];

    public function projects()
    {
        return $this->belongsTo('App\Models\Project');
    }
}
