<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectType extends Model
{
    protected $fillable = ['id','type','size_form','size_to','sale_from','sale_to','created_at','updated_at'];
    protected $relations =['projects'];

    public function projects()
    {
        return $this->belongsTo('App\Models\Project');
    }
}
