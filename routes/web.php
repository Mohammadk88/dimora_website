<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => LaravelLocalization::setLocale(),'as' => 'locale.', 'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]], function(){
    Route::get('/','MainController@index')->name('index');

    Route::get('/about-us/{aboutUs}','PagesController@aboutUs')->name('about-us');
    Route::get('/contact-us/{ContactUs}','PagesController@contactUs')->name('contact-us');
    Route::get('/faq/{faq}','PagesController@faq')->name('faq');
    Route::get('/citizenship/{citizenship}','PagesController@citizenship')->name('citizenship');

  //  Route::get('/flash','MainController@sessionFlash');

    Route::group(['prefix' => 'p/{type}','as' => 'project.'], function(){
        Route::get('/','ProjectController@index')->name('index');
        Route::get('s/{specificFeatures}/{specificFeatures_id}','CategoryController@specificFeatures');
        Route::group(['prefix' => 'c/{category}/{category_id}'], function(){
            Route::get('/','CategoryController@project');
            Route::get('/{id}/{title}','ProjectController@single');
        });
    });
    Route::group(['prefix' => 'b/{type}','as' => 'blog.'], function(){
        Route::get('/','BlogController@index')->name('index');
        Route::get('/{search}','BlogController@search');
        Route::group(['prefix' => 'c/{category}/{category_id}'], function(){
            Route::get('/','CategoryController@blog');
            Route::get('/{id}/{title}','BlogController@single');
        });
    });
});

