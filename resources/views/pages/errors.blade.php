@extends('layouts.main')
@section('title')
    {{__('main.error').' '.$code}}
@stop
@section('description')
    {{__('main.website_description')}}
@stop
@section('keywords')
    {{__('main.website_title')}},
@stop
@section('content')
    <!--  section  -->
    <section class="color-bg parallax-section">
        <div class="city-bg"></div>
        <div class="cloud-anim cloud-anim-bottom x1"><i class="fal fa-cloud"></i></div>
        <div class="cloud-anim cloud-anim-top x2"><i class="fal fa-cloud"></i></div>
        <div class="overlay op1 color3-bg"></div>
        <div class="container">
            <div class="error-wrap">
                <h2>{{$code}}</h2>
                <p>{{__('main.error_msg')}}</p>
                <div class="clearfix"></div>
                <a href="{{LaravelLocalization::localizeURL('/')}}" class="btn color2-bg flat-btn">{{__('main.back_to_home')}}<i class="fal fa-home"></i></a>
            </div>
        </div>
    </section>
    <!--  section  end-->
@stop
