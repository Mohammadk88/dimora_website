@extends('layouts.main')
@section('title')
    {{$current_breadcrumb['title']}}
@stop
@section('description')
    {{__('main.website_description')}}
@stop
@section('keywords')
    {{$current_breadcrumb['title']}},
    {{__('main.blog')}},
@stop
@section('content')
    <!-- map-view-wrap -->
    <div class="map-view-wrap">
        <div class="container">
            <div class="map-view-wrap_item">
                <div class="list-single-main-item-title fl-wrap">
                    <h3>{{$current_breadcrumb['title']}}</h3>
                </div>
                <div class="box-widget-list mar-top">
                    <ul>
                        <li><span><i class="fal fa-map-marker"></i> {{__('main.address')}} :</span>Çakmaklı, Avrupa Otoyolu Hadımköy Bağlantısı, 34500 Büyükçekmece/İstanbul</li>
                        <li><span><i class="fal fa-phone"></i> {{__('main.phone')}} :</span><a href="tel:+905523717308">+90 552 371 73 08</a></li>
                        <li><span><i class="fal fa-envelope"></i> {{__('main.email')}} :</span><a href="mailto:info@dimoraproperty.com" target="_blank">info@dimoraproperty.com</a></li>
                    </ul>
                </div>
                <div class="list-widget-social">
                    <ul>
                        <li><a href="https://www.facebook.com/Dimoraproperties" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="https://twitter.com/dimorapropertie" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="https://www.instagram.com/dimoraproperties/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--map-view-wrap end -->
    <!-- Map -->
    <div class="map-container fw-map2">
        <div id="singleMap" data-latitude="40.7427837" data-longitude="-73.11445617675781"></div>
    </div>
    <!-- Map end -->
    <div class="breadcrumbs-fs fl-wrap">
        <div class="container">
            <div class="breadcrumbs fl-wrap">
                @foreach($breadcrumbs as $breadcrumb)
                    <a href="{{$breadcrumb['url']}}" title="{{$breadcrumb['title']}}">{{$breadcrumb['title']}}</a>
                @endforeach
                <span> {{$current_breadcrumb['title']}}</span>
            </div>
        </div>
    </div>
    <section  id="sec1" class="grey-b lue-bg middle-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <!--   list-single-main-item -->
                    <div class="list-single-main-item fl-wrap">
                        <div class="list-single-main-item-title fl-wrap">
                            <h3>Our Office </h3>
                        </div>
                        <div class="list-single-main-media fl-wrap">
                            <img src="{{asset('images/pages/our_office.jpg')}}" alt="" class="respimg">
                        </div>
                        <p class="justify">{{__('main.contact_us_description')}}
                        </p>
                      {{--  <div class="list-single-main-item-title fl-wrap mar-top">
                            <h3>Working Hours </h3>
                        </div>
                        <ul class="cat-item">
                            <li><a href="#">Monday to Friday</a> <span>9am - 7pm</span></li>
                            <li><a href="#">Saturday to Sunday </a> <span>Closed</span></li>
                        </ul>--}}
                    </div>
                    <!--   list-single-main-item end -->
                </div>
                <div class="col-md-8">
                    <div class="list-single-main-item fl-wrap">
                        <div class="list-single-main-item-title fl-wrap">
                            <h3>{{__('main.Get_In_Touch')}}</h3>
                        </div>
                        <div id="contact-form">
                            <div id="message"></div>
                            <form  class="custom-form" action="php/contact.php" name="contactform" id="contactform">
                                <fieldset>
                                    <label><i class="fal fa-user"></i></label>
                                    <input type="text" name="name" id="name" placeholder="Your Name *" value=""/>
                                    <div class="clearfix"></div>
                                    <label><i class="far fa-phone"></i></label>
                                    <input type="text" name="phone" id="phone" placeholder="Your Phone Number *" value=""/>
                                    <label><i class="fal fa-envelope"></i>  </label>
                                    <input type="text"  name="email" id="email" placeholder="Email Address*" value=""/>

                                    <textarea name="comments"  id="comments" cols="40" rows="3" placeholder="Your Message:"></textarea>
                                </fieldset>
                                <button class="btn float-btn color2-bg" style="margin-top:15px;" id="submit">{{__('main.Send_Message')}}<i class="fal fa-angle-right"></i></button>
                            </form>
                        </div>
                        <!-- contact form  end-->
                    </div>
                </div>
            </div>
        </div>
        <div class="section-decor"></div>
    </section>
    <!-- section end -->
@stop
