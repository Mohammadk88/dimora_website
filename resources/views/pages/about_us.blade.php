@extends('layouts.main')
@section('title')
    {{$current_breadcrumb['title']}}
@stop
@section('description')
    {{__('config.description')}}
@stop
@section('keywords')
    {{$current_breadcrumb['title']}},
    {{__('main.blog')}},
@stop
@section('content')
    <!--  section  -->
    <section class="parallax-section single-par" data-scrollax-parent="true">
        <div class="bg par-elem "  data-bg="{{asset('images/pages/about_us_top.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
        <div class="overlay"></div>
        <div class="container">
            <div class="section-title center-align big-title">
                <div class="section-title-separator"><span></span></div>
                <h2><span> {{$current_breadcrumb['title']}}</span></h2>
                <span class="section-separator"></span>
                <h4>{{__('config.description')}}</h4>
            </div>
        </div>
        <div class="header-sec-link">
            <div class="container"><a href="#sec1" class="custom-scroll-link color-bg"><i class="fal fa-angle-double-down"></i></a></div>
        </div>
    </section>
    <!--  section  end-->
    <div class="breadcrumbs-fs fl-wrap">
        <div class="container">
            <div class="breadcrumbs fl-wrap">
                @foreach($breadcrumbs as $breadcrumb)
                    <a href="{{$breadcrumb['url']}}" title="{{$breadcrumb['title']}}">{{$breadcrumb['title']}}</a>
                @endforeach
                <span> {{$current_breadcrumb['title']}}</span>
            </div>
        </div>
    </div>
    <section  id="sec1" class="middle-padding">
        <div class="container">
            <!--about-wrap -->
            <div class="about-wrap">
                <div class="row">
                    <div class="col-md-6">
                        <div class="video-box fl-wrap">
                            <img src="{{asset('images/pages/about_us_video.jpg')}}" class="respimg" alt="">
                           {{-- <a class="video-box-btn image-popup" href="https://vimeo.com/264074381"><i class="fa fa-play" aria-hidden="true"></i></a>
                            <span class="video-box-title">Our Video Presentation</span>--}}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="list-single-main-item-title fl-wrap">
                            <h3>{!! __('main.about_us_title') !!}</h3>
                        </div>
                        <p>{{__('main.about_us_description')}} </p>
{{--
                        <a href="#sec2" class="btn  color-bg float-btn custom-scroll-link">View Our Team <i class="fal fa-users"></i></a>
--}}
                    </div>
                </div>
            </div>
            <!-- about-wrap end  -->
            <div class=" single-facts fl-wrap ">
                <!-- inline-facts -->
                <div class="inline-facts-wrap">
                    <div class="inline-facts">
                        <i class="fal fa-users"></i>
                        <div class="milestone-counter">
                            <div class="stats animaper">
                                <div class="num" data-content="0" data-num="15000">15000</div>
                            </div>
                        </div>
                        <h6>{{__('main.counter_1')}}</h6>
                    </div>
                </div>
                <!-- inline-facts end -->
                <!-- inline-facts  -->
                <div class="inline-facts-wrap">
                    <div class="inline-facts">
                        <i class="fal fa-thumbs-up"></i>
                        <div class="milestone-counter">
                            <div class="stats animaper">
                                <div class="num" data-content="0" data-num="800">800</div>
                            </div>
                        </div>
                        <h6>{{__('main.counter_2')}}</h6>
                    </div>
                </div>
                <!-- inline-facts end -->
                <!-- inline-facts  -->
                <div class="inline-facts-wrap">
                    <div class="inline-facts">
                        <i class="fal fa-award"></i>
                        <div class="milestone-counter">
                            <div class="stats animaper">
                                <div class="num" data-content="0" data-num="7">7</div>
                            </div>
                        </div>
                        <h6>{{__('main.counter_3')}}</h6>
                    </div>
                </div>
                <!-- inline-facts end -->
                <!-- inline-facts  -->
                <div class="inline-facts-wrap">
                    <div class="inline-facts">
                        <i class="fal fa-hotel"></i>
                        <div class="milestone-counter">
                            <div class="stats animaper">
                                <div class="num" data-content="0" data-num="10">10</div>
                            </div>
                        </div>
                        <h6>{{__('main.counter_4')}}</h6>
                    </div>
                </div>
                <!-- inline-facts end -->
            </div>
        </div>
    </section>
    <!-- section end -->
    <!--section -->
    <section class="color-bg hidden-section">
        <div class="wave-bg wave-bg2"></div>
        <div class="container">
            <div class="section-title">
                <h2>{{__('main.Why_Choose_Us')}}</h2>
                <span class="section-separator"></span>
                <p>{{__('main.some_of_our_services')}}</p>
            </div>
            <!-- -->
            <div class="row">
                <div class="col-md-4">
                    <!-- process-item-->
                    <div class="process-item big-pad-pr-item">
                        <span class="process-count"> </span>
                        <div class="time-line-icon"><i class="fal fa-suitcase"></i></div>
                        <h4><a href="javascript:;" title="{{__('main.service_1')}}">{{__('main.service_1')}} </a></h4>
                        <p class="justify">{{__('main.service_1_description')}}</p>
                    </div>
                    <!-- process-item end -->
                </div>
                <div class="col-md-4">
                    <!-- process-item-->
                    <div class="process-item big-pad-pr-item">
                        <span class="process-count"> </span>
                        <div class="time-line-icon"><i class="fal fa-clock"></i></div>
                        <h4> <a href="javascript:;" title="{{__('main.service_2')}}">{{__('main.service_2')}}</a></h4>
                        <p class="justify">{{__('main.service_2_description')}}</p>
                    </div>
                    <!-- process-item end -->
                </div>
                <div class="col-md-4">
                    <!-- process-item-->
                    <div class="process-item big-pad-pr-item nodecpre">
                        <span class="process-count"> </span>
                        <div class="time-line-icon"><i class="fal fa-award"></i></div>
                        <h4><a href="javascript:;" title="{{__('main.service_3')}}"> {{__('main.service_3')}}</a></h4>
                        <p class="justify">{{__('main.service_3_description')}} </p>
                    </div>
                    <!-- process-item end -->
                </div>
            </div>
            <!--process-wrap   end-->
        </div>
    </section>
    <!-- section end -->
    <!--section -->
   {{-- <section id="sec2">
        <div class="container">
            <div class="section-title">
                <h2>Our Team</h2>
                <span class="section-separator"></span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar.</p>
            </div>
            <div class="team-holder section-team fl-wrap">
                <!-- team-item -->
                <div class="team-box">
                    <div class="team-photo">
                        <img src="{{asset('images/pages/team_1.jpg')}}" alt="" class="respimg">
                    </div>
                    <div class="team-info">
                        <div class="team-dec"><i class="fal fa-chart-line"></i></div>
                        <h3><a href="#">Alisa Gray</a></h3>
                        <h4>Business consultant</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  </p>
                        <div class="team-social">
                            <ul>
                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                            </ul>
                            <a class="team-contact_link" href="#"><i class="fal fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
                <!-- team-item  end-->
                <!-- team-item -->
                <div class="team-box">
                    <div class="team-photo">
                        <img src="{{asset('images/pages/team_2.jpg')}}" alt="" class="respimg">
                    </div>
                    <div class="team-info">
                        <div class="team-dec"><i class="fal fa-camera-retro"></i></div>
                        <h3><a href="#">Austin Evon</a></h3>
                        <h4>Photographer</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  </p>
                        <div class="team-social">
                            <ul>
                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                            </ul>
                            <a class="team-contact_link" href="#"><i class="fal fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
                <!-- team-item end  -->
                <!-- team-item -->
                <div class="team-box">
                    <div class="team-photo">
                        <img src="{{asset('images/pages/team_3.jpg')}}" alt="" class="respimg">
                    </div>
                    <div class="team-info">
                        <div class="team-dec"><i class="fal fa-desktop"></i></div>
                        <h3><a href="#">Taylor Roberts</a></h3>
                        <h4>Co-manager associated</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  </p>
                        <div class="team-social">
                            <ul>
                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                            </ul>
                            <a class="team-contact_link" href="#"><i class="fal fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
                <!-- team-item end  -->
            </div>
        </div>
    </section>--}}
    <!-- section end -->
    <section class="parallax-section" data-scrollax-parent="true">
        <div class="bg par-elem "  data-bg="{{asset('images/pages/bg.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
        <div class="overlay"></div>
        <!--container-->
        <div class="container">
            <div class="intro-item fl-wrap">
                <h2>{{__('main.Contact_your_real_estate_expert')}}</h2>
{{--
                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</h3>
--}}
                <a class="btn  color-bg" href="{{LaravelLocalization::localizeURL('contact-us/'.slugfy(__('main.links.contactUs')))}}" title="{{__('main.ContactUs')}}">Get in Touch <i class="fal fa-envelope"></i></a>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="section-title">
                <h2>{{__('main.testimonials')}}</h2>
                <span class="section-separator"></span>
                <p>{{__('main.testimonials_description')}}</p>
            </div>
        </div>
        <div class="clearfix"></div>
        <!--slider-carousel-wrap -->
        <div class="slider-carousel-wrap text-carousel-wrap fl-wrap">
            @if (count($testimonials) > 3)
                <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
                <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>
            @endif
            <div class="text-carousel single-carousel cur_carousel-slider-container fl-wrap">
                @foreach($testimonials as $testimonial)
                <!--slick-item -->
                    <div class="slick-item">
                        <div class="text-carousel-item">
                            <div class="popup-avatar"><img src="{{asset(($testimonial['avatar'] != null) ? 'uploads/testimonial/'.$testimonial['avatar'] : 'assets/images/avatar/avatar-bg.png')}}" alt="{{$testimonial['name']}}"></div>
                            <div class="listing-rating card-popup-rainingvis" data-starrating2="5"> </div>
                            <div class="review-owner fl-wrap">{{$testimonial['name']}}  - <span>{{$testimonial['position']}}</span></div>
                            <p class="justify"> "{{$testimonial['text']}}"</p>
                            @if($testimonial['via'] != null)
                                <a href="javascript:;" class="testim-link" title="{{$testimonial['via']}}">{{$testimonial['via']}}</a>
                            @endif
                        </div>
                    </div>
                    <!--slick-item end -->
                @endforeach

            </div>
        </div>
        <!--slider-carousel-wrap end-->
        <div class="section-decor"></div>
    </section>
@stop
