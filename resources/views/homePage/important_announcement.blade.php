<section class="color-bg hidden-section">
    <div class="wave-bg wave-bg2"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <!-- -->
                <div class="colomn-text  pad-top-column-text fl-wrap">
                    <div class="colomn-text-title">
                        <h3>Our App   Available Now</h3>
                        <p>In ut odio libero, at vulputate urna. Nulla tristique mi a massa convallis cursus. Nulla eu mi magna. Etiam suscipit commodo gravida. Lorem ipsum dolor sit amet, conse ctetuer adipiscing elit, sed diam nonu mmy nibh euismod tincidunt ut laoreet dolore magna aliquam erat.</p>
                        <a href="#" class=" down-btn color3-bg"><i class="fab fa-apple"></i> Download for iPhone</a>
                        <a href="#" class=" down-btn color3-bg"><i class="fab fa-android"></i> Download for Android</a>
                    </div>
                </div>
                <!--process-wrap   end-->
            </div>
            <div class="col-md-6">
                <div class="collage-image">
                    <img src="{{asset('assets/images/api.png')}}" class="main-collage-image" alt="">
                    <div class="images-collage-title color3-bg">Dimora<span>Property</span></div>
                    <div class="collage-image-min cim_1"><img src="{{asset('assets/images/api/1.jpg')}}" alt=""></div>
                    <div class="collage-image-min cim_2"><img src="{{asset('assets/images/api/1.jpg')}}" alt=""></div>
                    <div class="collage-image-min cim_3"><img src="{{asset('assets/images/api/1.jpg')}}" alt=""></div>
                    <div class="collage-image-input">Search <i class="fa fa-search"></i></div>
                    <div class="collage-image-btn color2-bg">Booking now</div>
                </div>
            </div>
        </div>
    </div>
</section>
