<section id="sec2">
    <div class="container">
        <div class="section-title">
            <div class="section-title-separator"><span></span></div>
            <h2>{{__('main.comGate')}}</h2>
            <span class="section-separator"></span>
            <p>{{__('main.comGateDescription')}}</p>
        </div>
    </div>
    <!-- portfolio start -->
    <div class="gallery-items fl-wrap mr-bot spad home-grid four-column" style="height: auto !important; display: block !important;">
        @foreach($specificFeatures as $specificFeature_7)
        <!-- gallery-item-->
        <div class=" gallery-item {{ ($specificFeature_7['order'] == 3) ? 'gallery-item-second' : '' }}">
            <div class="grid-item-holder">
                <div class="listing-item-grid">
                    <div class="listing-counter">( <span>{{count($specificFeature_7['projects'])}} </span> ) &nbsp;{{__('main.project')}}</div>
                    <a href="{{LaravelLocalization::localizeURL('/').'/p/'.__('main.projects').'/s/'.slugfy($specificFeature_7['title']).'/'.$specificFeature_7['id']}}" title="{{$specificFeature_7['title']}}"><img  src="{{env('IMAGE_URL').'uploads/main/'.$specificFeature_7['photo']}}"   alt="{{$specificFeature_7['photo_alt']}}"></a>
                        <div class="listing-item-cat">
                        <h3><a href="{{LaravelLocalization::localizeURL('/').'/p/'.__('main.projects').'/s/'.slugfy($specificFeature_7['title']).'/'.$specificFeature_7['id']}}" title="{{$specificFeature_7['title']}}">{{$specificFeature_7['title']}}</a></h3>
                        <div class="weather-grid"   data-grcity="{{$specificFeature_7['title']}}"></div>
                        <div class="clearfix"></div>
                        <p>{{$specificFeature_7['description']}}</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- gallery-item end-->
        @endforeach

    </div>
</section>
