<section>
    <div class="container">
        <div class="section-title">
            <div class="section-title-separator"><span></span></div>
            <h2>{{__('main.testimonials')}}</h2>
            <span class="section-separator"></span>
            <p>{{__('main.testimonials_description')}}</p>
        </div>
    </div>
    <div class="clearfix"></div>
    <!--slider-carousel-wrap -->
    <div class="slider-carousel-wrap text-carousel-wrap fl-wrap">
        @if (count($testimonials) > 3)
            <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
            <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>
        @endif
        <div class="text-carousel single-carousel fl-wrap">
            @foreach($testimonials as $testimonial)
            <!--slick-item -->
            <div class="slick-item">
                <div class="text-carousel-item">
                    <div class="popup-avatar"><img src="{{asset(($testimonial['avatar'] != null) ? 'uploads/testimonial/'.$testimonial['avatar'] : 'assets/images/avatar/avatar-bg.png')}}" alt="{{$testimonial['name']}}"></div>
                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"> </div>
                    <div class="review-owner fl-wrap">{{$testimonial['name']}}  - <span>{{$testimonial['position']}}</span></div>
                    <p class="justify"> "{{$testimonial['text']}}"</p>
                    @if($testimonial['via'] != null)
                    <a href="javascript:;" class="testim-link" title="{{$testimonial['via']}}">{{$testimonial['via']}}</a>
                    @endif
                </div>
            </div>
            <!--slick-item end -->
          @endforeach
        </div>
    </div>
    <!--slider-carousel-wrap end-->
</section>
