<section>
    <div class="container">
        <div class="section-title">
            <div class="section-title-separator"><span></span></div>
            <h2>{{__('main.Why_Choose_Us')}}</h2>
            <span class="section-separator"></span>
            <p>{{__('main.some_of_our_services')}}</p>
        </div>
        <!-- -->
        <div class="row">
            <div class="col-md-4">
                <!-- process-item-->
                <div class="process-item big-pad-pr-item">
                    <span class="process-count"> </span>
                    <div class="time-line-icon"><i class="fal fa-suitcase"></i></div>
                    <h4><a href="javascript:;" title="{{__('main.service_1')}}">{{__('main.service_1')}} </a></h4>
                    <p class="justify">{{__('main.service_1_description')}}</p>
                </div>
                <!-- process-item end -->
            </div>
            <div class="col-md-4">
                <!-- process-item-->
                <div class="process-item big-pad-pr-item">
                    <span class="process-count"> </span>
                    <div class="time-line-icon"><i class="fal fa-clock"></i></div>
                    <h4> <a href="javascript:;" title="{{__('main.service_2')}}">{{__('main.service_2')}}</a></h4>
                    <p class="justify">{{__('main.service_2_description')}}</p>
                </div>
                <!-- process-item end -->
            </div>
            <div class="col-md-4">
                <!-- process-item-->
                <div class="process-item big-pad-pr-item nodecpre">
                    <span class="process-count"> </span>
                    <div class="time-line-icon"><i class="fal fa-award"></i></div>
                    <h4><a href="javascript:;" title="{{__('main.service_3')}}"> {{__('main.service_3')}}</a></h4>
                    <p class="justify">{{__('main.service_3_description')}} </p>
                </div>
                <!-- process-item end -->
            </div>
        </div>
        <!--process-wrap   end-->
        <div class=" single-facts fl-wrap mar-top">
            <!-- inline-facts -->
            <div class="inline-facts-wrap">
                <div class="inline-facts">
                    <i class="fal fa-users"></i>
                    <div class="milestone-counter">
                        <div class="stats animaper">
                            <div class="num" data-content="0" data-num="15000">15000</div>
                        </div>
                    </div>
                    <h6>{{__('main.counter_1')}}</h6>
                </div>
            </div>
            <!-- inline-facts end -->
            <!-- inline-facts  -->
            <div class="inline-facts-wrap">
                <div class="inline-facts">
                    <i class="fal fa-thumbs-up"></i>
                    <div class="milestone-counter">
                        <div class="stats animaper">
                            <div class="num" data-content="0" data-num="800">800</div>
                        </div>
                    </div>
                    <h6>{{__('main.counter_2')}}</h6>
                </div>
            </div>
            <!-- inline-facts end -->
            <!-- inline-facts  -->
            <div class="inline-facts-wrap">
                <div class="inline-facts">
                    <i class="fal fa-award"></i>
                    <div class="milestone-counter">
                        <div class="stats animaper">
                            <div class="num" data-content="0" data-num="7">7</div>
                        </div>
                    </div>
                    <h6>{{__('main.counter_3')}}</h6>
                </div>
            </div>
            <!-- inline-facts end -->
            <!-- inline-facts  -->
            <div class="inline-facts-wrap">
                <div class="inline-facts">
                    <i class="fal fa-hotel"></i>
                    <div class="milestone-counter">
                        <div class="stats animaper">
                            <div class="num" data-content="0" data-num="10">10</div>
                        </div>
                    </div>
                    <h6>{{__('main.counter_4')}}</h6>
                </div>
            </div>
            <!-- inline-facts end -->
        </div>
    </div>
</section>
