<section class="grey-blue-bg">
    <!-- container-->
    <div class="container">
        <div class="section-title">
            <div class="section-title-separator"><span></span></div>
            <h2>{{__('main.last_project')}}</h2>
            <span class="section-separator"></span>
            <p>{{__('main.last_project_description')}}</p>
        </div>
    </div>
    <!-- container end-->
    <!-- carousel -->
    <div class="list-carousel fl-wrap card-listing ">
        <!--listing-carousel-->
        <div class="listing-carousel  fl-wrap ">
            @foreach($projects as $project)
            <!--slick-slide-item-->
            <div class="slick-slide-item">
                <!-- listing-item  -->
                <div class="listing-item">
                    <article class="geodir-category-listing fl-wrap">
                        <div class="geodir-category-img">
                            <a href="{{LaravelLocalization::localizeURL('p/'.__('main.projects').'/c/'.slugfy($project['Category']['title']).'/'.$project['Category']['id'].'/'.$project['id'].'/'.slugfy($project['title']))}}" title="{{$project['title']}}"><img src="{{env('IMAGE_URL').'uploads/project/'.$project['id'].'/'.$project['photos'][0]['file']}}" alt="{{$project['photos'][0]['photo_alt']}}"></a>
                           {{-- <div class="listing-avatar"><a href="author-single.html"><img src="images/avatar/1.jpg" alt=""></a>
                                <span class="avatar-tooltip">Added By  <strong>Alisa Noory</strong></span>
                            </div>--}}
                            <div class="sale-window">{{__('main.special_offer')}}</div>
                            {{--<div class="geodir-category-opt">
                                <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                <div class="rate-class-name">
                                    <div class="score"><strong>Very Good</strong>27 Reviews </div>
                                    <span>5.0</span>
                                </div>
                            </div>--}}
                        </div>
                        <div class="geodir-category-content fl-wrap title-sin_item">
                            <div class="geodir-category-content-title fl-wrap">
                                <div class="geodir-category-content-title-item">
                                    <h3 class="title-sin_map"><a href="{{LaravelLocalization::localizeURL('p/'.__('main.projects').'/c/'.slugfy($project['Category']['title']).'/'.$project['Category']['id'].'/'.$project['id'].'/'.slugfy($project['title']))}}" title="{{$project['title']}}">{{$project['title']}}</a></h3>
                                    <div class="geodir-category-location fl-wrap"><a href="javascript:;" class="map-item"><i class="fas fa-map-marker-alt"></i> {{$project['location']}}</a></div>
                                </div>
                            </div>
                            <p>{{$project['meta_description']}}</p>
                            <ul class="facilities-list fl-wrap">
                                @foreach($project['features'] as $features)
                                <li><i class="{{$features['icon']}}"></i><span>{{$features['name']}}</span></li>
                                @endforeach
                            </ul>
                            <div class="geodir-category-footer fl-wrap">
                                <div class="geodir-category-price">{{__('main.Pay_Cash')}} <span>${{$project['price']}}</span></div>
                              {{--  <div class="geodir-opt-list">
                                    <a href="javascript:;" class="single-map-item" data-newlatitude="40.72956781" data-newlongitude="-73.99726866"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">On the map</span></a>
                                    <a href="javascript:;" class="geodir-js-favorite"><i class="fal fa-heart"></i><span class="geodir-opt-tooltip">Save</span></a>
                                    <a href="javascript:;" class="geodir-js-booking"><i class="fal fa-exchange"></i><span class="geodir-opt-tooltip">Find Directions</span></a>
                                </div>--}}
                            </div>
                        </div>
                    </article>
                </div>
                <!-- listing-item end -->
            </div>
            <!--slick-slide-item end-->
           @endforeach
        </div>
        <!--listing-carousel end-->
        <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
        <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>
    </div>
    <!--  carousel end-->
</section>
