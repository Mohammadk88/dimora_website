<section class="parallax-section" data-scrollax-parent="true">
    <div class="bg"  data-bg="{{asset('images/main/project.jpg')}}" data-scrollax="properties: { translateY: '100px' }"></div>
    <div class="overlay op7"></div>
    <!--container-->
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="colomn-text fl-wrap pad-top-column-text_small">
                    <div class="colomn-text-title">
                        <h3>{{__('main.project_special')}}</h3>
                        <p>{{__('main.project_special_description')}}</p>
                        <a href="{{LaravelLocalization::localizeURL('p/'.__('main.projects'))}}" title="{{__('main.projects')}}" class="btn  color2-bg float-btn">{{__('main.view_all_projects')}}<i class="fas fa-caret-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <!--light-carousel-wrap-->
                <div class="light-carousel-wrap fl-wrap">
                    <!--light-carousel-->
                    <div class="light-carousel">
                        @foreach($projectSpecials as $projectSpecial)
                        <!--slick-slide-item-->
                        <div class="slick-slide-item">
                            <div class="hotel-card fl-wrap title-sin_item">
                                <div class="geodir-category-img card-post">
                                    <a href="{{LaravelLocalization::localizeURL('p/'.__('main.projects').'/c/'.slugfy($projectSpecial['Category']['title']).'/'.$projectSpecial['Category']['id'].'/'.$projectSpecial['id'].'/'.slugfy($projectSpecial['title']))}}" title="{{$projectSpecial['title']}}"><img src="{{env('IMAGE_URL').'uploads/project/'.$projectSpecial['id'].'/'.$projectSpecial['photos'][0]['file']}}" alt="{{$projectSpecial['photos'][0]['photo_alt']}}"></a>
                                    <div class="listing-counter">Pay Cash <strong>${{$projectSpecial['price']}}</strong></div>
                                    <div class="sale-window">{{__('main.special_offer')}}</div>
                                    <div class="geodir-category-opt">
{{--
                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
--}}
                                        <h4 class="title-sin_map"><a href="{{LaravelLocalization::localizeURL('p/'.__('main.projects').'/c/'.slugfy($projectSpecial['Category']['title']).'/'.$projectSpecial['Category']['id'].'/'.$projectSpecial['id'].'/'.slugfy($projectSpecial['title']))}}" title="{{$projectSpecial['title']}}">{{$projectSpecial['title']}}</a></h4>
                                        <div class="geodir-category-location"><a href="#" class="single-map-item" data-newlatitude="40.90261483" data-newlongitude="-74.15737152"><i class="fas fa-map-marker-alt"></i> {{$projectSpecial['location']}}</a></div>
                                        <div class="rate-class-name">
                                            <!-- div class="score"><strong> Good</strong>8 Reviews </div>
                                            <span>4.8</span -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--slick-slide-item end-->
                        @endforeach
                    </div>
                    <!--light-carousel end-->
                    <div class="fc-cont  lc-prev"><i class="fal fa-angle-left"></i></div>
                    <div class="fc-cont  lc-next"><i class="fal fa-angle-right"></i></div>
                </div>
                <!--light-carousel-wrap end-->
            </div>
        </div>
    </div>
</section>
