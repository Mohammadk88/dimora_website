<section class="parallax-section" data-scrollax-parent="true">
    <div class="bg"  data-bg="{{asset('images/main/contactus.jpg')}}" data-scrollax="properties: { translateY: '100px' }"></div>
    <div class="overlay"></div>
    <!--container-->
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <!-- column text-->
                <div class="colomn-text fl-wrap">
                    <div class="colomn-text-title">
                        <h3>The owner of the hotel or business ?</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar.</p>
                        <a href="dashboard-add-listing.html" class="btn  color-bg float-btn">Add your hotel<i class="fal fa-plus"></i></a>
                    </div>
                </div>
                <!--column text   end-->
            </div>
        </div>
    </div>
</section>
