<section style="direction: ltr" class="hero-section" data-scrollax-parent="true" id="sec1">
    <div class="hero-parallax">
        <div class="slideshow-container" data-scrollax="properties: { translateY: '200px' }" >
            <!-- slideshow-item -->
            @foreach($slides as $slide)
                <div class="slideshow-item">
                    <div class="bg"  data-bg="{{env('IMAGE_URL').'uploads/slide/'.$slide['photo']}}"></div>
                </div>
            @endforeach

            <!--  slideshow-item end  -->
        </div>
        <div class="overlay op7"></div>
    </div>
    <div class="hero-section-wrap fl-wrap">
        <div class="container">
            <div class="home-intro">
                <div class="section-title-separator"><span></span></div>
                <h2>{{__('main.slide.title')}}</h2>
                <span class="section-separator"></span>
                <h3>{{__('main.slide.details')}}</h3>
            </div>
            <div class="main-search-input-wrap">
               {{-- <div class="main-search-input fl-wrap">
                    <div class="main-search-input-item location" id="autocomplete-container">
                        <span class="inpt_dec"><i class="fal fa-map-marker"></i></span>
                        <input type="text" placeholder="Hotel , City..." class="autocomplete-input" id="autocompleteid2"  value=""/>
                        <a href="#"><i class="fal fa-dot-circle"></i></a>
                    </div>
                    <div class="main-search-input-item main-date-parent main-search-input-item_small">
                        <span class="inpt_dec"><i class="fal fa-calendar-check"></i></span> <input type="text" placeholder="When" name="main-input-search"   value=""/>
                    </div>
                    <div class="main-search-input-item">
                        <div class="qty-dropdown fl-wrap">
                            <div class="qty-dropdown-header fl-wrap"><i class="fal fa-users"></i> Persons</div>
                            <div class="qty-dropdown-content fl-wrap">
                                <div class="quantity-item">
                                    <label><i class="fas fa-male"></i> Adults</label>
                                    <div class="quantity">
                                        <input type="number" min="1" max="3" step="1" value="1">
                                    </div>
                                </div>
                                <div class="quantity-item">
                                    <label><i class="fas fa-child"></i> Children</label>
                                    <div class="quantity">
                                        <input type="number" min="0" max="3" step="1" value="0">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="main-search-button color2-bg" onclick="window.location.href='listing.html'">Search <i class="fal fa-search"></i></button>
                </div>--}}
            </div>
        </div>
    </div>
    <div class="header-sec-link">
        <div class="container"><a href="#sec2" class="custom-scroll-link color-bg"><i class="fal fa-angle-double-down"></i></a></div>
    </div>
</section>
