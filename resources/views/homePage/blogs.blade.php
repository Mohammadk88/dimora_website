<section class=" middle-padding">
    <div class="container">
        <div class="section-title">
            <div class="section-title-separator"><span></span></div>
            <h2>{{__('main.articles')}}</h2>
            <span class="section-separator"></span>
            <p>{{__('main.articles_description')}}</p>
        </div>
        <div class="row home-posts">
            @foreach($blogs as $blog)
            <div class="col-md-4">
                <article class="card-post">
                    <div class="card-post-img fl-wrap">
                        <a href="{{LaravelLocalization::localizeURL($blog['category']['type'][0].'/'.__('main.blog').'/c/'.slugfy($blog['category']['title']).'/'.$blog['category']['id']).'/'.$blog['id'].'/'.slugfy($blog['title'])}}" title="{{$blog['title']}}"><img  src="{{env('IMAGE_URL').'uploads/blog/'.$blog['photo']}}"   alt=""></a>
                    </div>
                    <div class="card-post-content fl-wrap">
                        <h3><a href="{{LaravelLocalization::localizeURL($blog['category']['type'][0].'/'.__('main.blog').'/c/'.slugfy($blog['category']['title']).'/'.$blog['category']['id']).'/'.$blog['id'].'/'.slugfy($blog['title'])}}" title="{{$blog['title']}}">{{$blog['title']}}</a></h3>
                        <p>{{$blog['meta_description']}} </p>
{{--
                        <div class="post-author"><a href="#"><img src="images/avatar/1.jpg" alt=""><span>By , Mery Lynn</span></a></div>
--}}
                        <div class="post-opt">
                            <ul>
                                <li><i class="fal fa-calendar"></i> <span>{{$blog['created_at']}}</span></li>
                                <li><i class="fal fa-eye"></i> <span>{{$blog['view']}}</span></li>
                                <li><i class="fal fa-tags"></i> <a href="{{LaravelLocalization::localizeURL($blog['category']['type'][0].'/'.__('main.blog').'/c/'.slugfy($blog['category']['title']).'/'.$blog['category']['id'])}}" title="{{$blog['category']['title']}}">{{$blog['category']['title']}}</a>  </li>
                            </ul>
                        </div>
                    </div>
                </article>
            </div>
           @endforeach
        </div>
        <a href="{{LaravelLocalization::localizeURL('b/'.slugfy(__('main.articles')))}}" class="btn    color-bg " title="{__('main.articles')}">{{__('main.view_all_articles')}}<i class="fas fa-caret-right"></i></a>
    </div>
    <div class="section-decor"></div>
</section>
