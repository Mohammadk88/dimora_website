@extends('layouts.main')
@section('title')
    {{$current_breadcrumb['title']}}
@stop
@section('description')
    {{$project['meta_description']}}
@stop
@section('keywords')
    {{$project['tags']}},
    {{$project['keyword']}},
    {{$project['title']}}
@stop
@section('content')
    <!--  section  -->
    <section class="grey-blue-bg small-padding scroll-nav-container">
        <div class="top-dec"></div>
        <!--  scroll-nav-wrapper  -->
        <div class="scroll-nav-wrapper fl-wrap">
            {{--            <div class="hidden-map-container fl-wrap">
                            <input id="pac-input" class="controls fl-wrap controls-mapwn" type="text"
                                   placeholder="What Nearby ?   Bar , Gym , Restaurant ">
                            <div class="map-container">
                                <div id="singleMap" data-latitude="40.7427837" data-longitude="-73.11445617675781"></div>
                            </div>
                        </div>--}}

            <div class="clearfix"></div>
            <div class="container">
                <nav class="scroll-nav scroll-init">
                    <ul>
                        <li><a class="act-scrlink" href="#sec1">{{__('category.project.Gallery')}}</a></li>
                        <li><a href="#sec2">{{__('category.project.Details')}}</a></li>
                        <li><a href="#sec3">{{__('category.project.Amenities')}}</a></li>
                        <li><a href="#sec4">{{__('category.project.Rooms')}}</a></li>
                        {{--
                                                <li><a href="#sec5">{{__('category.project.Reviews')}}</a></li>
                        --}}
                    </ul>
                </nav>
                {{--
                                <a href="#" class="show-hidden-map"> <span>On The Map</span> <i class="fal fa-map-marked-alt"></i></a>
                --}}
            </div>
        </div>
        <!--  scroll-nav-wrapper end  -->
        <!--   container  -->
        <div class="container max-width-100">
            <div class="row">
                <div class="col-md-8">
                    <div class="list-single-main-container ">
                        <!-- fixed-scroll-column  -->
                        <div class="fixed-scroll-column">
                            <div class="fixed-scroll-column-item fl-wrap">
                                <div class="showshare sfcs fc-button"><i
                                        class="far fa-share-alt"></i><span>{{__('category.project.shear')}} </span>
                                </div>
                                <div class="share-holder fixed-scroll-column-share-container">
                                    <div class="share-container  isShare"></div>
                                </div>
                                {{--
                                                                <a class="fc-button custom-scroll-link" href="#sec6"><i
                                                                        class="far fa-comment-alt-check"></i> <span>  Add review </span></a>
                                                                <a class="fc-button" href="#"><i class="far fa-heart"></i> <span>Save</span></a>
                                                                <a class="fc-button" href="booking-single.html"><i class="far fa-bookmark"></i> <span> Book Now </span></a>
                                --}}
                            </div>
                        </div>
                        <!-- fixed-scroll-column end   -->
                        <div class="list-single-main-media fl-wrap" id="sec1" style="direction: ltr">
                            <div class="gallery-items grid-small-pad  list-single-gallery three-coulms lightgallery">
                                <div class="slider-for fl-wrap">
                                    @foreach($project['photos'] as $photo)
                                        <div class="gallery-item">
                                            <div class="grid-item-holder">
                                                <div class="box-item">
                                                    <img
                                                        src="{{env('IMAGE_URL').'uploads/project/'.$project['id'].'/'.$photo['file']}}"
                                                        alt="{{$photo['photo_alt']}}">
                                                    <a href="{{env('IMAGE_URL').'uploads/project/'.$project['id'].'/'.$photo['file']}}"
                                                       class="gal-link popup-image"><i class="fa fa-search"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="single-slider-wrapper fl-wrap">
                                <div class="slider-nav fl-wrap">
                                    @foreach($project['photos'] as $photo)
                                        <div class="slick-slide-item"><img
                                                src="{{env('IMAGE_URL').'uploads/project/'.$project['id'].'/thumbnail/'.$photo['file']}}"
                                                alt="{{$photo['photo_alt']}}"></div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list-single-main-container ">
                        <!-- list-single-header end -->
                        <div class="list-single-facts fl-wrap" id="sec2">
                            <!-- inline-facts -->
                            <div class="inline-facts-wrap">
                                <div class="inline-facts">
                                    <i class="fal fa-tags "></i>
                                    <div class="milestone-counter">
                                        <div class="stats animaper">
                                            <h6>
                                                <a href="{{LaravelLocalization::localizeURL('p/'.__('main.projects').'/c/'.slugfy($project['Category']['title']).'/'.$project['Category']['id'].'/')}}"
                                                   title="{{$project['category']['title']}}">{{$project['category']['title']}}</a>
                                            </h6>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- inline-facts end -->
                            <!-- inline-facts  -->
                            <div class="inline-facts-wrap">
                                <div class="inline-facts">
                                    <i class="fal fa-map-marker"></i>
                                    <div class="milestone-counter">
                                        <div class="stats animaper">
                                            <h6>{{$project['location']}}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- inline-facts end -->
                            <!-- inline-facts -->
                            <div class="inline-facts-wrap">
                                <div class="inline-facts">
                                    <i class="fal fa-chart-line"></i>
                                    <div class="milestone-counter">
                                        <div class="stats animaper">
                                            <h4>{{$project['status']}}%</h4>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- inline-facts end -->
                            <!-- inline-facts -->
                            <div class="inline-facts-wrap">
                                <div class="inline-facts">
                                    <i class="fal fa-calendar-check"></i>
                                    <div class="milestone-counter">
                                        <div class="stats animaper">
                                            <h6>{{$project['delivery_date']}}</h6>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- inline-facts end -->
                        </div>
                        <!--   list-single-main-item -->
                        <div class="list-single-main-item fl-wrap">
                            <div class="list-single-main-item-title fl-wrap">
                                <h3>{{$project['title']}} </h3>
                            </div>
                            {!! $project['description'] !!}

                            {{--<a href="https://vimeo.com/166396229" class="btn flat-btn color-bg big-btn float-btn image-popup">Video Presentation <i class="fal fa-play"></i></a>--}}
                        </div>
                        <!--   list-single-main-item end -->
                        <!--   list-single-main-item -->
                        <div class="list-single-main-item fl-wrap" id="sec3">
                            <div class="list-single-main-item-title fl-wrap">
                                <h3>{{__('category.project.Amenities')}}</h3>
                            </div>
                            <div class="listing-features fl-wrap">
                                <ul>
                                    @foreach($project['features'] as $features)
                                        <li><i class="{{$features['icon']}}"></i>{{$features['name']}}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <span class="fw-separator"></span>
                            <div class="list-single-main-item-title no-dec-title fl-wrap">
                                <h3>{{__('category.Tags')}}</h3>
                            </div>
                            <div class="list-single-tags tags-stylwrap">
                                @foreach(explode(',',$project['tags']) as $tag)
                                    <a href="#">{{$tag}}</a>
                                @endforeach
                            </div>
                        </div>
                        <!--   list-single-main-item end -->
                        <!-- accordion-->
                        <div class="accordion mar-top">
                            @foreach($project['details'] as $details)
                                <a class="toggle  {{($details['open']) ? 'act-accordion' : '' }}"
                                   href="#"> {{$details['title']}} <span></span></a>
                                <div class="accordion-inner {{($details['open']) ? 'visible' : '' }} ">
                                    {{$details['description']}}
                                </div>
                            @endforeach
                        </div>
                        <!-- accordion end -->
                        <!--   list-single-main-item -->
                        <div class="list-single-main-item fl-wrap" id="sec4">
                            <div class="list-single-main-item-title fl-wrap">
                                <h3>{{__('category.project.Rooms')}} </h3>
                            </div>
                            <!--   rooms-container -->
                            <div class="rooms-container fl-wrap">
                            @foreach($project['types'] as $types)
                                <!--  rooms-item -->
                                    <div class="rooms-item fl-wrap">
                                            <div class="rooms-details row width-100">
                                                {{-- <div class="rooms-details-header fl-wrap">
                                                     <span class="rooms-price">$81 <strong> / person</strong></span>
                                                     <h3>Standard Family Room</h3>
                                                     <h5>Max Guests: <span>3 persons</span></h5>
                                                 </div>--}}
                                                <div class="col-md-6 col-sm-12">
                                                    <span class="title">{{__('category.project.type')}}</span>
                                                    &nbsp; : &nbsp;
                                                    <span class="description">{{$types['type']}}</span>
                                                    &nbsp; &nbsp; &nbsp; - &nbsp; &nbsp; &nbsp;
                                                    <span class="title"> {{__('category.project.SizeFrom')}}</span>
                                                    &nbsp; : &nbsp;
                                                    <span
                                                        class="description"> {{ $types['size_form']}} &nbsp;{!! __('category.project.m2') !!} </span>
                                                    &nbsp; &nbsp; &nbsp; - &nbsp; &nbsp; &nbsp;
                                                    <span class="title">{{__('category.project.to')}}</span>
                                                    &nbsp; : &nbsp;
                                                    <span
                                                        class="description">{{$types['size_to']}} &nbsp;{!! __('category.project.m2') !!}</span>
                                                </div>
                                                <div class="col-md-6 col-sm-12">

                                                    <span class="title">{{__('category.project.PriceFrom')}}</span>
                                                    &nbsp; : &nbsp;
                                                    <span class="description">{{$types['sale_from']}}  &nbsp; $</span>
                                                    &nbsp; &nbsp; &nbsp; - &nbsp; &nbsp; &nbsp;
                                                    <span class="title">{{__('category.project.to')}}</span>
                                                    &nbsp; : &nbsp;
                                                    <span class="description">{{$types['sale_to']}} &nbsp; $</span>
                                                </div>

                                            </div>
                                    </div>
                                    <!--  rooms-item end -->
                                @endforeach
                            </div>
                            <!--   rooms-container end -->
                        </div>
                        <!-- list-single-main-item end -->
                        <!-- list-single-main-item -->
                    {{--  <div class="list-single-main-item fl-wrap" id="sec5">
                          <div class="list-single-main-item-title fl-wrap">
                              <h3>Item Reviews - <span> 2 </span></h3>
                          </div>
                          <div class="reviews-comments-wrap">
                              <!-- reviews-comments-item -->
                              <div class="reviews-comments-item">
                                  <div class="review-comments-avatar">
                                      <img src="images/avatar/1.jpg" alt="">
                                  </div>
                                  <div class="reviews-comments-item-text">
                                      <h4><a href="#">Liza Rose</a></h4>
                                      <div class="review-score-user">
                                          <span>4.4</span>
                                          <strong>Good</strong>
                                      </div>
                                      <div class="clearfix"></div>
                                      <p>" Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla
                                          consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec,
                                          vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis
                                          vitae, justo. Nullam dictum felis eu pede mollis pretium. "</p>
                                      <div class="reviews-comments-item-date"><span><i
                                                  class="far fa-calendar-check"></i>12 April 2018</span><a href="#"><i
                                                  class="fal fa-reply"></i> Reply</a></div>
                                  </div>
                              </div>
                              <!--reviews-comments-item end-->
                              <!-- reviews-comments-item -->
                              <div class="reviews-comments-item">
                                  <div class="review-comments-avatar">
                                      <img src="images/avatar/1.jpg" alt="">
                                  </div>
                                  <div class="reviews-comments-item-text">
                                      <h4><a href="#">Adam Koncy</a></h4>
                                      <div class="review-score-user">
                                          <span>4.7</span>
                                          <strong>Very Good</strong>
                                      </div>
                                      <div class="clearfix"></div>
                                      <p>" Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc posuere
                                          convallis purus non cursus. Cras metus neque, gravida sodales massa ut.
                                          "</p>
                                      <div class="reviews-comments-item-date"><span><i
                                                  class="far fa-calendar-check"></i>03 December 2017</span><a
                                              href="#"><i class="fal fa-reply"></i> Reply</a></div>
                                  </div>
                              </div>
                              <!--reviews-comments-item end-->
                          </div>
                      </div>--}}
                    <!-- list-single-main-item end -->
                        <!-- list-single-main-item -->
                    {{-- <div class="list-single-main-item fl-wrap" id="sec6">
                         <div class="list-single-main-item-title fl-wrap">
                             <h3>Add Review</h3>
                         </div>
                         <!-- Add Review Box -->
                         <div id="add-review" class="add-review-box">
                             <!-- Review Comment -->
                             <form id="add-comment" class="add-comment  custom-form" name="rangeCalc">
                                 <fieldset>
                                     <div class="review-score-form fl-wrap">
                                         <div class="review-range-container">
                                             <!-- review-range-item-->
                                             <div class="review-range-item">
                                                 <div class="range-slider-title">Cleanliness</div>
                                                 <div class="range-slider-wrap ">
                                                     <input type="text" class="rate-range" data-min="0" data-max="5"
                                                            name="rgcl" data-step="1" value="4">
                                                 </div>
                                             </div>
                                             <!-- review-range-item end -->
                                             <!-- review-range-item-->
                                             <div class="review-range-item">
                                                 <div class="range-slider-title">Comfort</div>
                                                 <div class="range-slider-wrap ">
                                                     <input type="text" class="rate-range" data-min="0" data-max="5"
                                                            name="rgcl" data-step="1" value="1">
                                                 </div>
                                             </div>
                                             <!-- review-range-item end -->
                                             <!-- review-range-item-->
                                             <div class="review-range-item">
                                                 <div class="range-slider-title">Staf</div>
                                                 <div class="range-slider-wrap ">
                                                     <input type="text" class="rate-range" data-min="0" data-max="5"
                                                            name="rgcl" data-step="1" value="5">
                                                 </div>
                                             </div>
                                             <!-- review-range-item end -->
                                             <!-- review-range-item-->
                                             <div class="review-range-item">
                                                 <div class="range-slider-title">Facilities</div>
                                                 <div class="range-slider-wrap">
                                                     <input type="text" class="rate-range" data-min="0" data-max="5"
                                                            name="rgcl" data-step="1" value="3">
                                                 </div>
                                             </div>
                                             <!-- review-range-item end -->
                                         </div>
                                         <div class="review-total">
                                             <span><input type="text" name="rg_total" value=""
                                                          data-form="AVG({rgcl})" value="0"></span>
                                             <strong>Your Score</strong>
                                         </div>
                                     </div>
                                     <div class="row">
                                         <div class="col-md-6">
                                             <label><i class="fal fa-user"></i></label>
                                             <input type="text" placeholder="Your Name *" value=""/>
                                         </div>
                                         <div class="col-md-6">
                                             <label><i class="fal fa-envelope"></i> </label>
                                             <input type="text" placeholder="Email Address*" value=""/>
                                         </div>
                                     </div>
                                     <textarea cols="40" rows="3" placeholder="Your Review:"></textarea>
                                 </fieldset>
                                 <button class="btn  big-btn flat-btn float-btn color2-bg" style="margin-top:30px">
                                     Submit Review <i class="fal fa-paper-plane"></i></button>
                             </form>
                         </div>
                         <!-- Add Review Box / End -->
                     </div>--}}
                    <!-- list-single-main-item end -->
                    </div>
                </div>
                <div class="col-md-4">
                    <!--  flat-hero-container -->
                    <div class="flat-hero-container fl-wrap">
                        <div class="box-widget-item-header fl-wrap ">
                            <h3>{{$project['title']}}</h3>
                            <div class="listing-rating-wrap">
                                <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                            </div>
                        </div>
                        <div class="list-single-hero-price fl-wrap">{{__('category.project.AveragePrice')}}
                            <span> $ {{$project['price']}}</span></div>
                        <!--reviews-score-wrap-->
                    {{--<div class="reviews-score-wrap fl-wrap">
                        <div class="rate-class-name-wrap fl-wrap">
                            <div class="rate-class-name">
                                <span>4.5</span>
                                <div class="score"><strong>Very Good</strong>2 Reviews</div>
                            </div>
                            <a href="#sec6" class="color-bg  custom-scroll-link">Add Review <i
                                    class="far fa-comment-alt-check"></i></a>
                        </div>
                        <div class="review-score-detail">
                            <!-- review-score-detail-list-->
                            <div class="review-score-detail-list">
                                <!-- rate item-->
                                <div class="rate-item fl-wrap">
                                    <div class="rate-item-title fl-wrap"><span>Cleanliness</span></div>
                                    <div class="rate-item-bg" data-percent="100%">
                                        <div class="rate-item-line color-bg"></div>
                                    </div>
                                    <div class="rate-item-percent">5.0</div>
                                </div>
                                <!-- rate item end-->
                                <!-- rate item-->
                                <div class="rate-item fl-wrap">
                                    <div class="rate-item-title fl-wrap"><span>Comfort</span></div>
                                    <div class="rate-item-bg" data-percent="90%">
                                        <div class="rate-item-line color-bg"></div>
                                    </div>
                                    <div class="rate-item-percent">5.0</div>
                                </div>
                                <!-- rate item end-->
                                <!-- rate item-->
                                <div class="rate-item fl-wrap">
                                    <div class="rate-item-title fl-wrap"><span>Staf</span></div>
                                    <div class="rate-item-bg" data-percent="80%">
                                        <div class="rate-item-line color-bg"></div>
                                    </div>
                                    <div class="rate-item-percent">4.0</div>
                                </div>
                                <!-- rate item end-->
                                <!-- rate item-->
                                <div class="rate-item fl-wrap">
                                    <div class="rate-item-title fl-wrap"><span>Facilities</span></div>
                                    <div class="rate-item-bg" data-percent="90%">
                                        <div class="rate-item-line color-bg"></div>
                                    </div>
                                    <div class="rate-item-percent">4.5</div>
                                </div>
                                <!-- rate item end-->
                            </div>
                            <!-- review-score-detail-list end-->
                        </div>
                    </div>--}}
                    <!-- reviews-score-wrap end -->
                        {{-- <a href="booking-single.html" class="btn flat-btn color2-bg big-btn float-btn image-popup">Book
                             Now<i class="far fa-bookmark"></i></a>--}}
                    </div>
                    <!--   flat-hero-container end -->
                    <!--box-widget-wrap -->
                    <div class="box-widget-wrap">


                        <!--box-widget-item -->
                        <div class="box-widget-item fl-wrap"></div>
                        <!--box-widget-item end -->
                        <!--box-widget-item -->
                        <div class="box-widget-item fl-wrap">
                            <div class="box-widget">
                                <div class="box-widget-content">
                                    <div class="box-widget-item-header">
                                        <h3> {{__('category.project.ContactInformation')}}</h3>
                                    </div>
                                    <div class="box-widget-list">
                                        <ul>
                                            <li><span><i class="fal fa-map-marker"></i> {{__('category.project.Address')}}  :</span>
                                                <a href="javascript:;"
                                                   title="{{$project['location']}}">{{$project['location']}}</a></li>
                                            <li><span><i
                                                        class="fal fa-phone"></i> {{__('category.project.Phone')}} :</span>
                                                <a href="tel:{{$project['phone']}}"
                                                   title="{{$project['phone']}}">{{$project['phone']}}</a>
                                            </li>
                                            <li><span><i class="fal fa-envelope"></i>{{__('category.project.Mail')}}  :</span>
                                                <a target="_blank" href="mailto:{{$project['email']}}"
                                                   title="{{$project['email']}}">{{$project['email']}}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--box-widget-item end -->
                        <!--box-widget-item -->
                        <div class="box-widget-item fl-wrap">
                            <div class="box-widget">
                                <div class="box-widget-content">
                                    <div class="box-widget-item-header" style="text-align: left">
                                        <h3> {{__('category.project.booking')}}</h3>
                                        <span>{{__('category.project.bookingDescription')}}</span>
                                    </div>
                                    <form name="bookFormCalc" class="book-form custom-form">
                                        <fieldset>
                                            <div class="cal-item">
                                                <div class="bookdate-container  fl-wrap">
                                                    <label>{{__('category.project.name')}}<i class="far fa-user"></i>  </label>
                                                    <input type="text" name="name" value="" />
                                                </div>
                                            </div>
                                            <div class="cal-item">
                                                <div class="bookdate-container  fl-wrap">
                                                    <label>{{__('category.project.Mail')}}<i class="far fa-envelope"></i>  </label>
                                                    <input type="text" name="email" value="" />
                                                </div>
                                            </div>
                                            <div class="cal-item">
                                                <div class="bookdate-container  fl-wrap">
                                                    <label>{{__('category.project.Phone')}}<i class="far fa-phone"></i>  </label>
                                                    <input type="text" name="phone" value="" />
                                                </div>
                                            </div>
                                            <div class="profile-edit-container">
                                                <div class="custom-form">
                                                    <label>{{__('category.project.description')}}</label>
                                                    <textarea cols="40" rows="3" name="description" placeholder="Datails"></textarea>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <button class="btnaplly color2-bg">{{__('category.project.send')}}<i class="fal fa-paper-plane"></i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--box-widget-item end -->

                        <!--box-widget-item -->
                      {{--  <div class="box-widget-item fl-wrap">
                            <div class="box-widget widget-posts">
                                <div class="box-widget-content">
                                    <div class="box-widget-item-header">
                                        <h3>Recommended Attractions</h3>
                                    </div>
                                    <!--box-image-widget-->
                                    <div class="box-image-widget">
                                        <div class="box-image-widget-media"><img src="images/all/1.jpg" alt="">
                                            <a href="#" class="color2-bg" target="_blank">Details</a>
                                        </div>
                                        <div class="box-image-widget-details">
                                            <h4>Times Square <span>2.3 km</span></h4>
                                            <p>It’s impossible to miss the colossal billboards, glitzy lights and
                                                massive crowds that make this intersection the city’s beating heart.</p>
                                        </div>
                                    </div>
                                    <!--box-image-widget end -->
                                    <!--box-image-widget-->
                                    <div class="box-image-widget">
                                        <div class="box-image-widget-media"><img src="images/all/1.jpg" alt="">
                                            <a href="#" class="color2-bg" target="_blank">Details</a>
                                        </div>
                                        <div class="box-image-widget-details">
                                            <h4>Broadway<span>1.7 km</span></h4>
                                            <p>Tap your feet to catchy ditties, hold back tears or bust your gut
                                                laughing at a world renowned Broadway performance.</p>
                                        </div>
                                    </div>
                                    <!--box-image-widget end -->
                                    <!--box-image-widget-->
                                    <div class="box-image-widget">
                                        <div class="box-image-widget-media"><img src="images/all/1.jpg" alt="">
                                            <a href="#" class="color2-bg" target="_blank">Details</a>
                                        </div>
                                        <div class="box-image-widget-details">
                                            <h4>Grand Central Station<span>0.7 km</span></h4>
                                            <p>With its elegantly designed main concourse, this rail station is much
                                                more than just a massive transport hub.</p>
                                        </div>
                                    </div>
                                    <!--box-image-widget end -->
                                </div>
                            </div>
                        </div>--}}
                        <!--box-widget-item end -->
                        <!--box-widget-item -->
                        {{--<div class="box-widget-item fl-wrap">
                            <div class="box-widget">
                                <div class="box-widget-content">
                                    <div class="box-widget-item-header">
                                        <h3>Similar Listings</h3>
                                    </div>
                                    <div class="widget-posts fl-wrap">
                                        <ul>
                                            <li class="clearfix">
                                                <a href="#" class="widget-posts-img"><img src="images/gal/1.jpg"
                                                                                          class="respimg" alt=""></a>
                                                <div class="widget-posts-descr">
                                                    <a href="#" title="">Park Central</a>
                                                    <div class="listing-rating card-popup-rainingvis"
                                                         data-starrating2="5"></div>
                                                    <div class="geodir-category-location fl-wrap"><a href="#"><i
                                                                class="fas fa-map-marker-alt"></i> 40 JOURNAL SQUARE
                                                            PLAZA, NJ, US</a></div>
                                                    <span class="rooms-price">$80 <strong> /  Awg</strong></span>
                                                </div>
                                            </li>
                                            <li class="clearfix">
                                                <a href="#" class="widget-posts-img"><img src="images/gal/1.jpg"
                                                                                          class="respimg" alt=""></a>
                                                <div class="widget-posts-descr">
                                                    <a href="#" title="">Holiday Home</a>
                                                    <div class="listing-rating card-popup-rainingvis"
                                                         data-starrating2="3"></div>
                                                    <div class="geodir-category-location fl-wrap"><a href="#"><i
                                                                class="fas fa-map-marker-alt"></i> 75 PRINCE ST, NY, USA</a>
                                                    </div>
                                                    <span class="rooms-price">$50 <strong> /   Awg</strong></span>
                                                </div>
                                            </li>
                                            <li class="clearfix">
                                                <a href="#" class="widget-posts-img"><img src="images/gal/1.jpg"
                                                                                          class="respimg" alt=""></a>
                                                <div class="widget-posts-descr">
                                                    <a href="#" title="">Moonlight Hotel</a>
                                                    <div class="listing-rating card-popup-rainingvis"
                                                         data-starrating2="4"></div>
                                                    <div class="geodir-category-location fl-wrap"><a href="#"><i
                                                                class="fas fa-map-marker-alt"></i> 70 BRIGHT ST NEW
                                                            YORK, USA</a></div>
                                                    <span class="rooms-price">$105 <strong> /  Awg</strong></span>
                                                </div>
                                            </li>
                                        </ul>
                                        <a class="widget-posts-link" href="#">See All Listing <i
                                                class="fal fa-long-arrow-right"></i> </a>
                                    </div>
                                </div>
                            </div>
                        </div>--}}
                        <!--box-widget-item end -->
                    </div>
                    <!--box-widget-wrap end -->
                </div>
            </div>

        </div>
        <!--   container  end  -->
    </section>
    <!--  section  end-->
    <div class="limit-box fl-wrap"></div>
@stop

