@extends('layouts.main')
@section('title')
    {{__('main.main_page')}}
@stop
@section('description')
    {{__('main.website_description')}}
@stop
@section('content')
    <h1 style="display: none">{{__('main.main_page')}}</h1>
    <!--section -->
    @include('homePage.slide')
    <!-- section end -->
    <!--section -->
    @include('homePage.5_tags')
    <!-- section end -->
    <!-- section-->
    @include('homePage.projects')
    <!-- section end -->
    <!--section -->
    @include('homePage.special_project')
    <!-- section end -->
    <!--section -->
    @include('homePage.services_counter')
    <!-- section end -->
    <!--section -->
{{--
    @include('homePage.important_announcement')
--}}
    <!-- section end -->
    <!--section -->
    @include('homePage.testimonials')
    <!-- section end-->
{{--
    @include('homePage.provide_advice')
--}}
    <!-- section end -->
    <!--section -->
    @include('homePage.blogs')
@stop
