@extends('layouts.main')
@section('title')
    {{$current_breadcrumb['title']}}
@stop
@section('description')
    {{$category['description']}}
@stop
@section('keywords')
    {{$category['tags']}},
    {{$category['keyword']}},
    {{$category['title']}}
@stop
@section('content')
    <!--  section  end-->
    <div class="breadcrumbs-fs fl-wrap">
        <div class="container">
            <div class="breadcrumbs fl-wrap">
                @foreach($breadcrumbs as $breadcrumb)
                    <a href="{{$breadcrumb['url']}}">{{$breadcrumb['title']}}</a>
                @endforeach
                <span> {{$current_breadcrumb['title']}}</span>
            </div>
        </div>
    </div>
    <!--section -->
    <section  id="sec1" class="middle-padding grey-blue-bg">
        <div class="container max-width-100">
            <div class="row">
                <!--blog content -->
                <div class="col-md-9">
                    <!--post-container -->
                    <div class="post-container fl-wrap">
                        @foreach($blogs as $blog)
                        <!--article-masonry -->
                        <div class="article-masonry">
                            <article class="card-post">
                                <div class="card-post-img fl-wrap">
                                    <a href="{{LaravelLocalization::localizeURL($blog['category']['type'][0].'/'.__('main.blog').'/c/'.slugfy($blog['category']['title']).'/'.$blog['category']['id']).'/'.$blog['id'].'/'.slugfy($blog['title'])}}" title="{{$blog['title']}}"><img src="{{env('IMAGE_URL').'uploads/blog/thumbnail/'.$blog['photo']}}" alt="{{$blog['photo_alt']}}"></a>
                                </div>
                                <div class="card-post-content fl-wrap">
                                    <h3><a href="{{LaravelLocalization::localizeURL($blog['category']['type'][0].'/'.__('main.blog').'/c/'.slugfy($blog['category']['title']).'/'.$blog['category']['id']).'/'.$blog['id'].'/'.slugfy($blog['title'])}}" title="{{$blog['title']}}">{{$blog['title']}}</a></h3>
                                    <p>{{$blog['meta_description']}} </p>
                                    {{--<div class="post-author"><a href="#"><img src="images/avatar/1.jpg" alt=""><span>By , Mery Lynn</span></a></div>--}}
                                    <div class="post-opt">
                                        <ul>
                                            <li><i class="fal fa-calendar"></i> <span>{{$blog['created_at']}}</span></li>
                                            <li><i class="fal fa-eye"></i> <span>{{$blog['view']}}</span></li>
                                            <li><i class="fal fa-tags"></i> <a href="{{LaravelLocalization::localizeURL($blog['category']['type'][0].'/'.__('main.blog').'/c/'.slugfy($blog['category']['title']).'/'.$blog['category']['id'])}}">{{$blog['category']['title']}}</a>  </li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <!--article-masonry end -->
                        @endforeach

                        <!-- pagination-->
                        <div class="pagination">
                            @if (count($blogs) > 0)
                                {{$blogs->links()}}
                            @endif
                        </div>
                        <!-- pagination end-->
                    </div>
                    <!--post-container end -->
                </div>
                <!-- blog content end -->
                <!--   sidebar  -->
                <div class="col-md-3">
                    <!--box-widget-wrap -->
                    <div class="box-widget-wrap fl-wrap">
                        <!--box-widget-item -->
                        <div class="box-widget-item fl-wrap">
                            <div class="box-widget">
                                <div class="box-widget-content">
                                    <div class="box-widget-item-header">
                                        <h3> {{__('category.search_blog')}} </h3>
                                    </div>
                                    <div class="search-widget">
                                        <div action="#" class="fl-wrap">
                                            <input type="hidden" name="search_url" id="search_url" value="{{LaravelLocalization::localizeURL('b/'.__('main.blog').'/')}}">
                                            <input name="search" id="search" type="text" class="search" placeholder="{{__('category.search')}}..."  />
                                            <button onclick="search()" class="search-submit color2-bg" ><i class="fal fa-search transition"></i> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--box-widget-item end -->
                       {{-- <!--box-widget-item -->
                        <div class="box-widget-item fl-wrap">
                            <div class="box-widget">
                                <div class="box-widget-content">
                                    <div class="box-widget-item-header">
                                        <h3>About Athor</h3>
                                    </div>
                                    <div class="box-widget-author fl-wrap">
                                        <div class="box-widget-author-title fl-wrap">
                                            <div class="box-widget-author-title-img">
                                                <img src="images/avatar/1.jpg" alt="">
                                            </div>
                                            <a href="author-single.html">Jessie Manrty</a>
                                            <span>Co-manager associated</span>
                                        </div>
                                        <div class="box-widget-author-content">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla.</p>
                                        </div>
                                        <div class="list-widget-social">
                                            <ul>
                                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--box-widget-item end -->--}}
                        <!--box-widget-item -->
                        <div class="box-widget-item fl-wrap">
                            <div class="box-widget widget-posts">
                                <div class="box-widget-content">
                                    <div class="box-widget-item-header">
                                        <h3>{{__('category.Popular_Posts')}}</h3>
                                    </div>
                                    @foreach($Popular_Posts as $Popular_Post)
                                    <!--box-image-widget-->
                                    <div class="box-image-widget">
                                        <div class="box-image-widget-media"><img src="{{env('IMAGE_URL').'uploads/blog/thumbnail/'.$Popular_Post['photo']}}" alt="{{$Popular_Post['photo_alt']}}">
                                            <a href="{{LaravelLocalization::localizeURL($Popular_Post['category']['type'][0].'/'.__('main.blog').'/c/'.$Popular_Post['category']['slug'].'/'.$Popular_Post['category']['id']).'/'.$Popular_Post['id'].'/'.slugfy($Popular_Post['title'])}}" title="{{$Popular_Post['title']}}" class="color-bg">{{__('category.Details')}}</a>
                                        </div>
                                        <div class="box-image-widget-details">
                                            <h4>{{$Popular_Post['title']}}</h4>
                                            <p class="article-p">{{$Popular_Post['meta_description']}}</p>
                                            <span class="widget-posts-date"><i class="fal fa-calendar"></i>{{$Popular_Post['created_at']}}</span>
                                        </div>
                                    </div>
                                    <!--box-image-widget end -->
                                    @endforeach

                                </div>
                            </div>
                        </div>
                        <!--box-widget-item end -->
                        <!--box-widget-item -->
                        <div class="box-widget-item fl-wrap">
                            <div class="box-widget">
                                <div class="box-widget-content">
                                    <div class="box-widget-item-header">
                                        <h3>{{__('category.Tags')}} </h3>
                                    </div>
                                    <div class="list-single-tags tags-stylwrap  sb-tags">
                                        @foreach ($tags as $tag)
                                            <a href="#">{{$tag['tag']}}</a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--box-widget-item end -->
                        <!--box-widget-item -->
                        <div class="box-widget-item fl-wrap">
                            <div class="box-widget">
                                <div class="box-widget-content">
                                    <div class="box-widget-item-header">
                                        <h3> {{__('category.categories')}}</h3>
                                    </div>
                                    <ul class="cat-item">
                                        @foreach($categories as $category)
                                        <li><a href="{{LaravelLocalization::localizeURL($category['type'][0].'/'.__('main.blog').'/c/'.$category['slug'].'/'.$category['id'])}}">{{$category['title']}}</a> <span>{{count($category['blogs'])}}</span></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--box-widget-item end -->
                    </div>
                    <!--box-widget-wrap end -->
                </div>
                <!--   sidebar end  -->
            </div>
        </div>
        <div class="limit-box fl-wrap"></div>
    </section>
    <!-- section end -->
@stop
@section('script')
    <script type="text/javascript" src="{{asset('custom/js/blog.js')}}"></script>
@stop
