@extends('layouts.main')
@section('title')
    {{$current_breadcrumb['title']}}
@stop
@section('description')
    {{$category['description']}}
@stop
@section('keywords')
    {{$category['tags']}},
    {{$category['keyword']}},
    {{$category['title']}}
@stop
@section('content')
    <!--  section  end-->
    <div class="breadcrumbs-fs fl-wrap">
        <div class="container">
            <div class="breadcrumbs fl-wrap">
                @foreach($breadcrumbs as $breadcrumb)
                    <a href="{{$breadcrumb['url']}}">{{$breadcrumb['title']}}</a>
                @endforeach
                <span> {{$current_breadcrumb['title']}}</span>
            </div>
        </div>
    </div>
    <!--section -->
    <section  id="sec1" class="middle-padding grey-blue-bg">
        <div class="container max-width-100">
            <div class="row">
                <!--blog content -->
                <div class="col-md-9">
                    <!--post-container -->
                    <div class="post-container fl-wrap">
                        <!-- article> -->
                        <article class="post-article">
                            <div class="list-single-main-media fl-wrap">
                                <div class="single-slider-wrapper fl-wrap">
                                    <div class="single-slider fl-wrap"  >
                                        <div class="slick-slide-item"><img src="{{env('IMAGE_URL').'uploads/blog/'.$blog['photo']}}" alt="{{$blog['photo_alt']}}"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="list-single-main-item fl-wrap">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>{{$blog['title']}}</h3>
                                </div>
                                {!! $blog['description'] !!}
{{--
                                <div class="post-author"><a href="#"><img src="images/avatar/1.jpg" alt=""><span>By , Jessie Manrty</span></a></div>
--}}
                                <div class="post-opt">
                                    <ul>
                                        <li><i class="fal fa-calendar"></i> <span>{{$blog['created_at']}}</span></li>
                                        <li><i class="fal fa-eye"></i> <span>{{$blog['view']}}</span></li>
                                        <li><i class="fal fa-tags"></i> <a href="{{LaravelLocalization::localizeURL('b/'.__('main.blog').'/c/'.slugfy($blog['category']['title']).'/'.$blog['category']['id'])}}" title="{{$blog['category']['title']}}">{{$blog['category']['title']}}</a></li>
                                    </ul>
                                </div>
                                <span class="fw-separator"></span>
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>{{__('category.Tags')}}</h3>
                                </div>
                                <div class="list-single-tags tags-stylwrap blog-tags">
                                    @foreach(explode(',',$blog['tags']) as $tag)
                                    <a href="#">{{$tag}}</a>
                                    @endforeach
                                </div>
                               {{-- <span class="fw-separator"></span>--}}
                               {{-- <div class="post-nav fl-wrap">
                                    <a href="#" class="post-link prev-post-link"><i class="fal fa-angle-left"></i>Prev <span class="clearfix">The Sign of Experience</span></a>
                                    <a href="#" class="post-link next-post-link"><i class="fal fa-angle-right"></i>Next<span class="clearfix">Dedicated to Results</span></a>
                                </div>--}}
                            </div>
                            <!-- list-single-main-item -->
                           {{-- <div class="list-single-main-item fl-wrap" id="sec5">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>Comments -  <span> 2 </span></h3>
                                </div>
                                <div class="reviews-comments-wrap">
                                    <!-- reviews-comments-item -->
                                    <div class="reviews-comments-item">
                                        <div class="review-comments-avatar">
                                            <img src="images/avatar/1.jpg" alt="">
                                        </div>
                                        <div class="reviews-comments-item-text">
                                            <h4><a href="#">Liza Rose</a></h4>
                                            <div class="clearfix"></div>
                                            <p>" Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. "</p>
                                            <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>12 April 2018</span><a href="#"><i class="fal fa-reply"></i> Reply</a></div>
                                        </div>
                                    </div>
                                    <!--reviews-comments-item end-->
                                    <!-- reviews-comments-item -->
                                    <div class="reviews-comments-item">
                                        <div class="review-comments-avatar">
                                            <img src="images/avatar/1.jpg" alt="">
                                        </div>
                                        <div class="reviews-comments-item-text">
                                            <h4><a href="#">Adam Koncy</a></h4>
                                            <div class="clearfix"></div>
                                            <p>" Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc posuere convallis purus non cursus. Cras metus neque, gravida sodales massa ut. "</p>
                                            <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>03 December 2017</span><a href="#"><i class="fal fa-reply"></i> Reply</a></div>
                                        </div>
                                    </div>
                                    <!--reviews-comments-item end-->
                                </div>
                            </div>--}}
                            <!-- list-single-main-item end -->
                            <!-- list-single-main-item -->
                           {{-- <div class="list-single-main-item fl-wrap" id="sec6">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>Add Comment</h3>
                                </div>
                                <!-- Add Review Box -->
                                <div id="add-review" class="add-review-box">
                                    <!-- Review Comment -->
                                    <form id="add-comment" class="add-comment  custom-form" name="rangeCalc" >
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label><i class="fal fa-user"></i></label>
                                                    <input type="text" placeholder="Your Name *" value=""/>
                                                </div>
                                                <div class="col-md-6">
                                                    <label><i class="fal fa-envelope"></i>  </label>
                                                    <input type="text" placeholder="Email Address*" value=""/>
                                                </div>
                                            </div>
                                            <textarea cols="40" rows="3" placeholder="Your Review:"></textarea>
                                        </fieldset>
                                        <button class="btn  no-shdow-btn float-btn color2-bg" style="margin-top:30px">Submit Comment<i class="fal fa-paper-plane"></i></button>
                                    </form>
                                </div>
                                <!-- Add Review Box / End -->
                            </div>--}}
                            <!-- list-single-main-item end -->
                        </article>
                        <!-- article end -->
                    </div>
                    <!--post-container end -->
                </div>
                <!-- blog content end -->
                <!--   sidebar  -->
                <div class="col-md-3">
                    <!--box-widget-wrap -->
                    <div class="box-widget-wrap fl-wrap">
                        <!--box-widget-item -->
                        <div class="box-widget-item fl-wrap">
                            <div class="box-widget">
                                <div class="box-widget-content">
                                    <div class="box-widget-item-header">
                                        <h3> {{__('category.search_blog')}} </h3>
                                    </div>
                                    <div class="search-widget">
                                        <div action="#" class="fl-wrap">
                                            <input type="hidden" name="search_url" id="search_url" value="{{LaravelLocalization::localizeURL('b/'.__('main.blog').'/')}}">
                                            <input name="search" id="search" type="text" class="search" placeholder="{{__('category.search')}}..."  />
                                            <button onclick="search()" class="search-submit color2-bg" ><i class="fal fa-search transition"></i> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--box-widget-item end -->
                    {{-- <!--box-widget-item -->
                     <div class="box-widget-item fl-wrap">
                         <div class="box-widget">
                             <div class="box-widget-content">
                                 <div class="box-widget-item-header">
                                     <h3>About Athor</h3>
                                 </div>
                                 <div class="box-widget-author fl-wrap">
                                     <div class="box-widget-author-title fl-wrap">
                                         <div class="box-widget-author-title-img">
                                             <img src="images/avatar/1.jpg" alt="">
                                         </div>
                                         <a href="author-single.html">Jessie Manrty</a>
                                         <span>Co-manager associated</span>
                                     </div>
                                     <div class="box-widget-author-content">
                                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla.</p>
                                     </div>
                                     <div class="list-widget-social">
                                         <ul>
                                             <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                             <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                             <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                                             <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                         </ul>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <!--box-widget-item end -->--}}
                    <!--box-widget-item -->
                        <div class="box-widget-item fl-wrap">
                            <div class="box-widget widget-posts">
                                <div class="box-widget-content">
                                    <div class="box-widget-item-header">
                                        <h3>{{__('category.Popular_Posts')}}</h3>
                                    </div>
                                @foreach($Popular_Posts as $Popular_Post)
                                    <!--box-image-widget-->
                                        <div class="box-image-widget">
                                            <div class="box-image-widget-media"><img src="{{env('IMAGE_URL').'uploads/blog/thumbnail/'.$Popular_Post['photo']}}" alt="{{$Popular_Post['photo_alt']}}">
                                                <a href="{{LaravelLocalization::localizeURL($Popular_Post['category']['type'][0].'/'.__('main.blog').'/c/'.$Popular_Post['category']['slug'].'/'.$Popular_Post['category']['id']).'/'.$Popular_Post['id'].'/'.slugfy($Popular_Post['title'])}}" title="{{$Popular_Post['title']}}" class="color-bg">{{__('category.Details')}}</a>
                                            </div>
                                            <div class="box-image-widget-details">
                                                <h4>{{$Popular_Post['title']}}</h4>
                                                <p class="article-p">{{$Popular_Post['meta_description']}}</p>
                                                <span class="widget-posts-date"><i class="fal fa-calendar"></i>{{$Popular_Post['created_at']}}</span>
                                            </div>
                                        </div>
                                        <!--box-image-widget end -->
                                    @endforeach

                                </div>
                            </div>
                        </div>
                        <!--box-widget-item end -->
                        <!--box-widget-item -->
                        <div class="box-widget-item fl-wrap">
                            <div class="box-widget">
                                <div class="box-widget-content">
                                    <div class="box-widget-item-header">
                                        <h3>{{__('category.Tags')}} </h3>
                                    </div>
                                    <div class="list-single-tags tags-stylwrap  sb-tags">
                                        @foreach ($tags as $tag)
                                            <a href="#">{{$tag['tag']}}</a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--box-widget-item end -->
                        <!--box-widget-item -->
                        <div class="box-widget-item fl-wrap">
                            <div class="box-widget">
                                <div class="box-widget-content">
                                    <div class="box-widget-item-header">
                                        <h3> {{__('category.categories')}}</h3>
                                    </div>
                                    <ul class="cat-item">
                                        @foreach($categories as $category)
                                            <li><a href="{{LaravelLocalization::localizeURL($category['type'][0].'/'.__('main.blog').'/c/'.slugfy($category['title']).'/'.$category['id'])}}">{{$category['title']}}</a> <span>{{count($category['blogs'])}}</span></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--box-widget-item end -->
                    </div>
                    <!--box-widget-wrap end -->
                </div>
                <!--   sidebar end  -->
            </div>
        </div>
        <div class="limit-box fl-wrap"></div>
    </section>
    <!-- section end -->
@stop
@section('script')
    <script type="text/javascript" src="{{asset('custom/js/blog.js')}}"></script>
@stop
