<!-- header-top-->
<div class="header-top fl-wrap">
    <div class="container max-width-100">
        <div class="logo-holder">
            <a href="{{LaravelLocalization::localizeURL('/')}}"><img src="{{asset('images/logo/dimora-logo266_70.png')}}" alt=""></a>
        </div>
        <a href="javascript:;" class="add-projects">{{__('main.links.addYourProject')}}</a>
{{--
        <div class="show-reg-form modal-open"><i class="fa fa-sign-in"></i>Sign In (Soon)</div>
--}}
        <div class="lang-wrap">
            <div class="show-lang"> <span>{{getAllLang()[0][App::getLocale()]['name']}}</span><i class="fa fa-caret-down"></i></div>
            <ul class="lang-tooltip green-bg">
                @foreach(getAllLang()[0] as $index => $value)
                    @if($index == App::getLocale())
                        @continue
                    @endif
                    <li><a href="{{LaravelLocalization::getLocalizedURL($index)}}">{{$value['name']}}</a></li>
                @endforeach
            </ul>
        </div>
        {{--<div class="currency-wrap">
            <div class="show-currency-tooltip"><i style="margin-top: 4px;" class="fas {{session('currency')['icon']}}"></i> <span>{{session('currency')['key']}} <i class="fa fa-caret-down"></i> </span></div>
            <ul class="currency-tooltip">
                @foreach(session('currencies')[0] as $index => $value)
                    @if($index == session('currency')['key'])
                        @continue
                    @endif
                        <li><a href="{{route('currency',strtolower($index))}}"><i class="far {{$value['icon']}}"></i> {{$value['key']}}</a></li>
                @endforeach
            </ul>
        </div>--}}
    </div>
</div>
<!-- header-top end-->
<!-- header-inner-->
<div class="header-inner fl-wrap">
    <div class="container max-width-100">
       {{-- <div class="show-search-button"><span>{{__('main.links.search')}}</span> <i class="fas fa-search"></i> </div>--}}
{{--
        <div class="wishlist-link"><i class="fal fa-heart"></i><span class="wl_counter">3</span></div>
--}}
        {{--<div class="header-user-menu">
            <div class="header-user-name">
                <span><img src="images/avatar/1.jpg" alt=""></span>
                My account
            </div>
            <ul>
                <li><a href="dashboard-myprofile.html"> Edit profile</a></li>
                <li><a href="dashboard-add-listing.html"> Add Listing</a></li>
                <li><a href="dashboard-bookings.html">  Bookings  </a></li>
                <li><a href="dashboard-review.html"> Reviews </a></li>
                <li><a href="#">Log Out</a></li>
            </ul>
        </div>--}}
        <div class="home-btn "><a title="{{__('main.website_title')}}" href="{{LaravelLocalization::localizeURL('/')}}"><i class="fas fa-home"></i></a></div>
        <!-- nav-button-wrap-->
        <div class="nav-button-wrap color-bg">
            <div class="nav-button">
                <span></span><span></span><span></span>
            </div>
        </div>
        <!-- nav-button-wrap end-->
        <!--  navigation -->
        <div class="nav-holder main-menu">
            <nav>
                <ul>
                    <li>
                        <a href="{{LaravelLocalization::localizeURL('p/'.__('main.projects'))}}" class="{{ activeCheck('locale.project*') }}" title="{{__('main.links.project')}}">{{__('main.links.project')}} <i class="fas fa-caret-down"></i></a>
                        <!--second level -->
                        <ul>
                            @foreach(getProjectCategory() as $index => $ProjectCategory)
                            <li><a href="{{LaravelLocalization::localizeURL($ProjectCategory['type'][0].'/'.__('main.projects').'/c/'.$ProjectCategory['slug'].'/'.$index)}}" title="{{$ProjectCategory['title']}}">{{$ProjectCategory['title']}}</a></li>
                            @endforeach
                        </ul>
                        <!--second level end-->
                    </li>
                    <li>
                        <a href="{{LaravelLocalization::localizeURL('b/'.__('main.blog'))}}" class="{{ activeCheck('locale.blog*') }}" title="{{__('main.links.blog')}}">{{__('main.links.blog')}} <i class="fas fa-caret-down"></i></a>
                        <!--second level -->
                        <ul>
                            @foreach(getBlogCategory() as $index=>$BlogCategory)
                                <li><a href="{{LaravelLocalization::localizeURL($BlogCategory['type'][0].'/'.__('main.blog').'/c/'.$BlogCategory['slug'].'/'.$index)}}" title="{{$BlogCategory['title']}}">{{$BlogCategory['title']}}</a></li>
                            @endforeach
                        </ul>
                        <!--second level end-->
                    </li>
                    <li>
                        <a href="{{LaravelLocalization::localizeURL('about-us/'.slugfy(__('main.links.aboutUs')))}}" class="{{ activeCheck('locale.about-us') }}" title="{{__('main.links.aboutUs')}}">{{__('main.links.aboutUs')}} </a>
                    </li>
                    <li>
                        <a href="{{LaravelLocalization::localizeURL('contact-us/'.slugfy(__('main.links.contactUs')))}}" class="{{ activeCheck('locale.contact-us') }}" title="{{__('main.links.contactUs')}}">{{__('main.links.contactUs')}} </a>
                    </li>
                    <li>
                        <a href="{{LaravelLocalization::localizeURL('faq/'.slugfy(__('main.links.faq')))}}" class="{{ activeCheck('locale.faq') }}" title="{{__('main.links.faq')}}">{{__('main.links.faq')}} </a>
                    </li>
                    <li>
                        <a href="{{LaravelLocalization::localizeURL('citizenship/'.slugfy(__('main.links.citizenship')))}}" class="{{ activeCheck('locale.citizenship') }}" title="{{__('main.links.citizenship')}}" >{{__('main.links.citizenship')}} </a>
                    </li>
                   {{-- <li>
                        <a href="blog.html">{{__('main.links.provide-advice')}} </a>
                    </li>--}}
                </ul>
            </nav>
        </div>
        <!-- navigation  end -->
        <!-- wishlist-wrap-->
        <div class="wishlist-wrap scrollbar-inner novis_wishlist">
            <div class="box-widget-content">
                <div class="widget-posts fl-wrap">
                    <ul>
                        <li class="clearfix">
                            <a href="#"  class="widget-posts-img"><img src="{{asset('assets/images/gal/1.jpg')}}" class="respimg" alt=""></a>
                            <div class="widget-posts-descr">
                                <a href="#" title="">Park Central</a>
                                <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> 40 JOURNAL SQUARE PLAZA, NJ, US</a></div>
                                <span class="rooms-price">$80 <strong> /  Awg</strong></span>
                            </div>
                        </li>
                        <li class="clearfix">
                            <a href="#"  class="widget-posts-img"><img src="{{asset('assets/images/gal/1.jpg')}}" class="respimg" alt=""></a>
                            <div class="widget-posts-descr">
                                <a href="#" title="">Holiday Home</a>
                                <div class="listing-rating card-popup-rainingvis" data-starrating2="3"></div>
                                <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> 75 PRINCE ST, NY, USA</a></div>
                                <span class="rooms-price">$50 <strong> /   Awg</strong></span>
                            </div>
                        </li>
                        <li class="clearfix">
                            <a href="#"  class="widget-posts-img"><img src="{{asset('assets/images/gal/1.jpg')}}" class="respimg" alt=""></a>
                            <div class="widget-posts-descr">
                                <a href="#" title="">Moonlight Hotel</a>
                                <div class="listing-rating card-popup-rainingvis" data-starrating2="4"></div>
                                <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i>  70 BRIGHT ST NEW YORK, USA</a></div>
                                <span class="rooms-price">$105 <strong> /  Awg</strong></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- wishlist-wrap end-->
    </div>
</div>
<!-- header-inner end-->
<!-- header-search -->
<div class="header-search vis-search">
    <div class="container ">
        <div class="row">
            <!-- header-search-input-item -->
            <div class="col-sm-4">
                <div class="header-search-input-item fl-wrap location autocomplete-container">
                    <label>Destination or Hotel Name</label>
                    <span class="header-search-input-item-icon"><i class="fal fa-map-marker-alt"></i></span>
                    <input type="text" placeholder="Location" class="autocomplete-input" id="autocompleteid" value=""/>
                    <a href="#"><i class="fal fa-dot-circle"></i></a>
                </div>
            </div>
            <!-- header-search-input-item end -->
            <!-- header-search-input-item -->
            <div class="col-sm-3">
                <div class="header-search-input-item fl-wrap date-parent">
                    <label>Date In-Out </label>
                    <span class="header-search-input-item-icon"><i class="fal fa-calendar-check"></i></span>
                    <input type="text" placeholder="When" name="header-search"   value=""/>
                </div>
            </div>
            <!-- header-search-input-item end -->
            <!-- header-search-input-item -->
            <div class="col-sm-3">
                <div class="header-search-input-item fl-wrap">
                    <div class="quantity-item">
                        <label>Rooms</label>
                        <div class="quantity">
                            <input type="number" min="1" max="3" step="1" value="1">
                        </div>
                    </div>
                    <div class="quantity-item">
                        <label>Adults</label>
                        <div class="quantity">
                            <input type="number" min="1" max="3" step="1" value="1">
                        </div>
                    </div>
                    <div class="quantity-item">
                        <label>Children</label>
                        <div class="quantity">
                            <input type="number" min="0" max="3" step="1" value="0">
                        </div>
                    </div>
                </div>
            </div>
            <!-- header-search-input-item end -->
            <!-- header-search-input-item -->
            <div class="col-sm-2">
                <div class="header-search-input-item fl-wrap">
                    <button class="header-search-button" onclick="window.location.href='listing.html'">Search <i class="far fa-search"></i></button>
                </div>
            </div>
            <!-- header-search-input-item end -->
        </div>
    </div>
    <div class="close-header-search"><i class="fal fa-angle-double-up"></i></div>
</div>
<!-- header-search  end -->
