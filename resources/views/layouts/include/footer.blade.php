<!--subscribe-wrap-->
<div class="subscribe-wrap color-bg  fl-wrap">
    <div class="container">
        <div class="sp-bg"> </div>
        <div class="row">
            <div class="col-md-6">
                <div class="subscribe-header">
                    <h3>{{__('main.Subscribe')}}</h3>
                    <p>{{__('main.SubscribeDescription')}}</p>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <div class="footer-widget fl-wrap">
                    <div class="subscribe-widget fl-wrap">
                        <div class="subcribe-form">
                            <form id="subscribe">
                                <input class="enteremail fl-wrap" name="email" id="subscribe-email" placeholder="{{__('main.Enter_Your_Email')}}" spellcheck="false" type="text">
                                <button type="submit" id="subscribe-button" class="subscribe-button"><i class="fas fa-rss-square"></i> {{__('main.SubscribeNow')}}</button>
                                <label for="subscribe-email" class="subscribe-message"></label>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wave-bg"></div>
</div>
<!--subscribe-wrap end -->
<!--footer-inner-->
<div class="footer-inner">
    <div class="container max-width-100">
        <div class="row">
            <!--footer-widget -->
            <div class="col-md-3">
                <div class="footer-widget fl-wrap">
                    <h3>{{__('main.categories')}}</h3>
                    <div class="widget-posts fl-wrap">
                        <ul>
                            @foreach(getCategory() as $index=>$categories)
                                <li class="clearfix">
                                    <div class="widget-posts-descr">
                                        <a href="{{LaravelLocalization::localizeURL($categories['type'][0].'/'.__('main.'.$categories['type']).'/c/'.$categories['slug'].'/'.$index)}}" title="{{$categories['title']}}">{{$categories['title']}}</a>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!--footer-widget end-->
            <!--footer-widget -->
            <div class="col-md-3">
                <div class="footer-widget fl-wrap">
                    <h3>{{__('main.comGate')}}</h3>
                    <div class="widget-posts fl-wrap">
                        <ul>
                            @foreach(getSpecificFeatures() as $index=>$specificFeature)
                            <li class="clearfix">
                                <div class="widget-posts-descr">
                                    <a href="{{LaravelLocalization::localizeURL('/').'/p/'.__('main.projects').'/s/'.slugfy($specificFeature['title']).'/'.$index}}" title="{{$specificFeature['title']}}">{{$specificFeature['title']}}</a>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!--footer-widget end-->
            <!--footer-widget -->
            <div class="col-md-3">
                <div class="footer-widget fl-wrap">
                    <h3>{{__('main.ContactUs')}}</h3>

                    <div class="profile-edit-container">
                        <div class="custom-form">
                            <form>
                                <input type="text" placeholder="{{__('main.full_name')}}" value="">
                                <input type="text" placeholder="{{__('main.email')}}" value="">
                                <input type="text" placeholder="{{__('main.phone')}}" value="">
                                <textarea cols="20" rows="3" placeholder="{{__('main.message')}}"></textarea>
                                <button class="btn color2-bg float-btn" style="width: 100%;">{{__('main.send')}}<i class="fal fa-paper-plane"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--footer-widget end-->
            <!--footer-widget -->
            <div class="col-md-3">
                <div class="footer-widget fl-wrap">
                    <h3>{{__('main.our_location')}}</h3>
                    <div id="singleMap" data-latitude="41.0421143" data-longitude="28.626561"></div>
                </div>
            </div>
            <!--footer-widget end-->
        </div>
        <div class="clearfix"></div>
        <!--footer-widget -->
        <div class="footer-widget">
            <div class="row">
                <div class="col-md-6">
                    <div class="footer-widget fl-wrap">
                        <h3>{{__('main.links.aboutUs')}}</h3>
                        <div class="footer-contacts-widget fl-wrap">
                            <div class="row">
                                <div class="col-md-8">
                                    <p> {{__('config.description')}} </p>
                                    <div class="footer-social">
                                        <ul>
                                            <li><a href="https://www.facebook.com/Dimoraproperties" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="https://twitter.com/dimorapropertie" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="https://www.instagram.com/dimoraproperties/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <ul  class="footer-contacts fl-wrap">
                                        <li><span><i class="fal fa-envelope"></i> Mail :</span><a href="mailto:info@dimoraproperty.com" target="_blank">info@dimoraproperty.com</a></li>
                                        <li> <span><i class="fal fa-map-marker-alt"></i> Adress :</span><a href="javascript:;" target="_blank">Çakmaklı, Avrupa Otoyolu Hadımköy Bağlantısı, 34500 Büyükçekmece/İstanbul</a></li>
                                        <li><span><i class="fal fa-phone"></i> Phone :</span><a href="tel:+905523717308">+90 552 371 73 08</a></li>
                                    </ul>
                                </div>
                            </div>
                            </div>


                    </div>
                </div>



                <div class="col-md-6">
                    <div class="customer-support-widget fl-wrap">
                        <h4>{{__('main.Contact_consultant')}} : </h4>
                        <a href="tel:+905456297955" class="cs-mumber">+90 545 629 79 55</a>
                        <a href="tel:+905456297955" class="cs-mumber-button color2-bg">Call Now <i class="far fa-phone-volume"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!--footer-widget end -->
    </div>
</div>
<!--footer-inner end -->
<div class="footer-bg">
</div>
<!--sub-footer-->
<div class="sub-footer">
    <div class="container">
        <div class="copyright"> &#169; {{__('main.website_title')}} {{date('Y')}} .  {{__('main.All_rights_reserved')}}</div>
        <div class="subfooter-lang">
            <div class="subfooter-show-lang"><span>{{getAllLang()[0][App::getLocale()]['name']}}</span><i class="fa fa-caret-up"></i></div>
            <ul class="subfooter-lang-tooltip">
                @foreach(getAllLang()[0] as $index => $value)
                    @if($index == App::getLocale())
                        @continue
                    @endif
                        <li><a href="{{LaravelLocalization::getLocalizedURL($index)}}">{{$value['name']}}</a></li>
                @endforeach
            </ul>
        </div>
        <div class="subfooter-nav">
            <ul>
                <li><a href="#">{{__('main.Privacy_Policy')}}</a></li>
                <li><a href="{{LaravelLocalization::localizeURL('p/'.__('main.projects'))}}" title="{{__('main.links.project')}}">{{__('main.links.project')}}</a></li>
                <li><a href="{{LaravelLocalization::localizeURL('b/'.slugfy(__('main.articles')))}}" title="{{__('main.articles')}}">{{__('main.articles')}}</a></li>
                <li><a href="{{LaravelLocalization::localizeURL('contact-us/'.slugfy(__('main.links.contactUs')))}}" title="{{__('main.ContactUs')}}">{{__('main.ContactUs')}}</a></li>
                <li><a href="{{LaravelLocalization::localizeURL('about-us/'.slugfy(__('main.links.aboutUs')))}}" title="{{__('main.links.aboutUs')}}">{{__('main.links.aboutUs')}}</a></li>
                <li><a href="{{LaravelLocalization::localizeURL('faq/'.slugfy(__('main.links.faq')))}}" title="{{__('main.links.faq')}}">{{__('main.links.faq')}}</a></li>
            </ul>
        </div>
    </div>
</div>
<!--sub-footer end -->
