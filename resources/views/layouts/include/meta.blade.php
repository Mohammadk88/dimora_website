<!--=============== basic  ===============-->
<meta charset="UTF-8">
<script async src="https://cdn.ampproject.org/v0.js"></script>
<title>
    {{__('main.website_title')}} | @yield('title')
</title>
<link rel="canonical" href="{{url()->current()}}">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

<script type="application/ld+json">
{
    "@context": {{url()->current()}},
    "@type": "{{__('main.website_title')}} | @yield('title')",
    "headline": " @yield('description')",
    "datePublished": "2020-3-01T12:02:41Z",
    "image": [
        "{{asset('images/logo/logo.png')}}"
    ]
 }
    </script>
<style amp-boilerplate>
    body {
        -webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
        -moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
        -ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
        animation: -amp-start 8s steps(1, end) 0s 1 normal both;
    }
    @-webkit-keyframes -amp-start {
        from {
            visibility: hidden;
        }
        to {
            visibility: visible;
        }
    }
    @-moz-keyframes -amp-start {
        from {
            visibility: hidden;
        }
        to {
            visibility: visible;
        }
    }
    @-ms-keyframes -amp-start {
        from {
            visibility: hidden;
        }
        to {
            visibility: visible;
        }
    }
    @-o-keyframes -amp-start {
        from {
            visibility: hidden;
        }
        to {
            visibility: visible;
        }
    }
    @keyframes -amp-start {
        from {
            visibility: hidden;
        }
        to {
            visibility: visible;
        }
    }
</style>
<noscript>
    <style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style>
</noscript>
<meta name="robots" content="index, follow"/>
<meta name="keywords" content=" {{__('main.website_title')}}, @yield('keywords')"/>
<meta name="description" content=" @yield('description')"/>
<!--=============== css  ===============-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/reset.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/plugins.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/style.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/color.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('custom/css/main.css')}}">
<link href="{{asset('custom/font.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset("custom/css/responsive.css")}}" rel="stylesheet" type="text/css" />
@if(__('config.dir') == 'rtl')
    <link href="{{asset('custom/css/rtl.css')}}" rel="stylesheet" type="text/css" />
@endif
<!--=============== favicons ===============-->
<link rel="shortcut icon" href="{{asset('favicon.ico')}}">
