<script type="text/javascript" src="{{asset('assets/js/jquery.min.js')}}"></script>
@include('layouts.include.config_script')
<script type="text/javascript" src="{{asset('assets/js/plugins.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/scripts.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5FGBNPIaOOhdtlAfV9Llbqs49v8ly9HY&libraries=places&callback=initAutocomplete"></script>
<script type="text/javascript" src="{{asset('assets/js/map-single.js')}}"></script>
@routes
@yield('script')
