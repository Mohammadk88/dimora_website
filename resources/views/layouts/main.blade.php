<!DOCTYPE HTML>
<html amp direction="{{__('config.dir')}}"  style="direction: {{__('config.dir')}}" lang="{{App::getLocale()}}">
<head>
    @include('layouts.include.meta')
</head>
<body>
<!--loader-->
<div class="loader-wrap">
    <div class="pin">
        <div class="pulse"></div>
    </div>
</div>
<!--loader end-->
<!-- Main  -->
<div id="main">
    <!-- header-->
    <header class="main-header">
        @include('layouts.include.main-header')
    </header>
    <!--  header end -->
    <!--  wrapper  -->
    <div id="wrapper">
        <!-- content-->
        <div class="content">
           @yield('content')
        </div>
        <!-- content end-->
    </div>
    <!--wrapper end -->
    <!--footer -->
    <footer class="main-footer">
        @include('layouts.include.footer')
    </footer>
    <!--footer end -->
    <!--map-modal -->
        @include('layouts.include.map-modal')
    <!--map-modal end -->
    <!--register form -->
        @include('layouts.include.register-modal')
    <!--register form end -->
    <a class="to-top"><i class="fas fa-caret-up"></i></a>
</div>
<!-- Main end -->
<!--=============== scripts  ===============-->
@include('layouts.include.script')
</body>
</html>
