<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Config Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'dir' => 'rtl',
    'webTitle' => 'نظام ديمورا',
    'description' => 'Management of real estate projects, tourism and real estate articles, and everything related to that. It also contains the personnel management system',
    'soon' => 'قريبا',
    'pending' => 'بالانتظار',
    'thisDay' => 'هذا اليوم',
    'login' => 'تسجيل الدخول',
    'welcome_to' => ' مرحبا  إلى ',
    'sing_in' => 'تسجيل الدخول',
    'add' => 'إضافة',
    'edit' => 'تعديل',
    'delete' => 'حذف',
    'save' => 'حفظ',
    'back' => 'عودة',
    'search' => 'بحث',
    'other_lang' => 'لغات أخرى',
    'next_step' => 'الخطوة التالية',
    'backToIndex' => 'عودة إلى الصفحة الرئيسية',
    'month' => [
        'January' => 'كانون الثاني',
        'February' => 'شباط',
        'March' => 'آذار',
        'April' => 'نيسان',
        'May' => 'أيار',
        'June' => 'حزيران',
        'July' => 'تموز',
        'August' => 'آب',
        'September' => 'ايلول',
        'October' => 'تشرين الأول',
        'November' => 'تشرين الثاني',
        'December' => 'كانون الأول',
    ]
];
