<?php

return [
    'results_for' => 'النتائح لــ',
    'search_blog' => 'البحث في المقالات',
    'search' => 'البحث',
    'Popular_Posts' => 'منشورات شائعة',
    'Details' => 'التفاصيل',
    'Tags' => 'الوسوم',
    'categories' => 'التصنيفات',
    'project' => [
        'Gallery' => 'معرص الصور',
        'Details' => 'التفاصيل',
        'Amenities' => 'الميزات',
        'Rooms' => 'الغرف',
        'Reviews' => 'التقييم',
        'shear' => 'المشاركة',
        'type' => 'النوع',
        'SizeFrom' => 'الحجم من',
        'to' => 'إلى',
        'PriceFrom' => 'السعر من',
        'm2' => 'متر مربع',
        'AveragePrice' => 'السعر الوسطي',
        'ContactInformation' => 'معلوماات الاتصال',
        'Address' => 'العنوان',
        'Phone' => 'الهاتف',
        'Mail' => 'البريد الالكتروني',
        'booking' => 'حجز',
        'bookingDescription' => 'ارسل رسالة إلينا وسنقوم بالتواصل معك',
        'name' => 'الاسم الكامل',
        'description' => 'التفاصيل',
        'send' => 'ارسال',

    ]
];
