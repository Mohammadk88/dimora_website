<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Config Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'dir' => 'ltr',
    'webTitle' => 'Dimora Sistem',
    'description' => 'Management of real estate projects, tourism and real estate articles, and everything related to that. It also contains the personnel management system',
    'soon' => 'Yakında',
    'pending' => 'Bekliyor',
    'thisDay' => 'Bugün',
    'login' => 'Oturum aç',
    'welcome_to' => 'Hoşgeldiniz',
    'sing_in' => 'Oturum aç',
    'add' => 'Ekle',
    'edit' => 'Düzenle',
    'delete' => 'Silmek',
    'search' => 'Ara',
    'other_lang' => 'Diğer diller',
    'save' => 'Kaydet',
    'back' => 'Geri',
    'next_step' => 'Bir sonraki adım',
    'backToIndex' => 'Dizine Geri Dön',
    'month' => [
        'January' => 'Ocak',
        'February' => 'Şubat',
        'March' => 'Mart',
        'April' => 'Nisan',
        'May' => 'Mayıs',
        'June' => 'Haziran',
        'July' => 'Temmuz',
        'August' => 'Ağustos',
        'September' => 'Eylül',
        'October' => 'Ekim',
        'November' => 'Kasım',
        'December' => 'Aralık',
    ]
];
