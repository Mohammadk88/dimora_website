<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Website Main Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'links' => [
        'provide-advice' => 'Gayrimenkul danışmanlığı',
        'citizenship' => 'Yatırımla Türk Vatandaşlığı',
        'search' => 'Ara',
        'project' => 'Projeler',
        'blog' => 'Türkiye\'ye yatırım hakkında haberler',
        'aboutUs' => 'Dimora Hakkında',
        'contactUs' => 'Bize Ulaşın',
        'faq' => 'SSS',
        'addYourProject' => 'Projelerinizi Ekleyin (Yakında)'
    ],
    'slide'=>[
        'title' => 'Yapay zeka ile çalışan Dimora Sistemi size yardımcı olacaktır',
        'details' => 'Sizin için neyin doğru olduğunu düşündüğümüzü görün. Başkalarında görmediğiniz sürece Dimora\'da göreceksiniz'
    ]
];
