<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Config Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'dir' => 'ltr',
    'webTitle' => 'Dimora System',
    'description' => 'We have been in the property business for years and have accumulated a lot of experience through the years which can be useful in your search for a good investment opportunity.',
    'soon' => 'Soon',
    'pending' => 'Pending',
    'thisDay' => 'This Day',
    'login' => 'Login',
    'welcome_to' => 'Welcome to',
    'sing_in' => 'Login',
    'add' => 'Add',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'search' => 'Search',
    'other_lang' => 'Other Language',
    'save' => 'Save',
    'back' => 'Back',
    'next_step' => 'Next Step',
    'backToIndex' => 'Back to Index',
    'month' => [
        'January' => 'January',
        'February' => 'February',
        'March' => 'March',
        'April' => 'April',
        'May' => 'May',
        'June' => 'June',
        'July' => 'July',
        'August' => 'August',
        'September' => 'September',
        'October' => 'October',
        'November' => 'November',
        'December' => 'December',
    ]

];
