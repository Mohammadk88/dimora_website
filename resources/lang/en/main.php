<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Website Main Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'website_title' => 'Dimora Property',
    'home' => 'Home',
    'error' => 'Error',
    'error_msg' => 'We\'re sorry, but the Page you were looking for, have a temporary problem, please try again later.',
    'All_rights_reserved' =>'All rights reserved',
    'back_to_home' => 'Back to Home',
    'website_description' => 'Real estate system based on artificial intelligence to determine what suits the customer from a property and taking into account the price, type, category and everything related to other specifications',
    'main_page' => 'Home',
    'project' => 'Project',
    'projects' => 'Projects',
    'blog' => 'Blog',
    'help_center' => 'frequently asked questions',
    'comGate' => 'Company Gates',
    'comGateDescription' => 'Help you choose what you want by showing an entrance to the most wanted options in Turkey',
    'links' => [
        'provide-advice' => 'Real estate consulting',
        'citizenship' => 'Turkish Citizenship by Investment',
        'search' => 'Search',
        'project' => 'Projects',
        'blog' => 'News about investing in Turkey',
        'aboutUs' => 'About Dimora',
        'contactUs' => 'Contact Us',
        'faq' => 'FAQ',
        'addYourProject' => 'Add Your Projects (Soon)',
        'our_services' => 'Our Services'
    ],
    'slide'=>[
        'title' => 'Dimora System powered by AI will help you',
        'details' => 'Start by seeing what we thought was right for you. You will see in Dimora unless you see it in others'
    ],
    'last_project' => 'Recently Added Projects',
    'last_project_description' => 'Existing projects are carefully selected',
    'special_offer' => 'Special Offer',
    'project_special' => 'Our distinguished projects',
    'project_special_description' => 'They are selected by Top Turkish Real estate Market Experts ',
    'view_all_projects' => 'View all projects',
    'Why_Choose_Us' => 'Why should you choose us',
    'some_of_our_services' => 'Some of the services we provide and some statistics about our work',
    'articles' => 'Tips & Articles',
    'articles_description' => 'We put articles and tips that are of interest to every investor. We will not sell you a headband, but we will tell you how to benefit from it',
    'view_all_articles' => 'View All Articles',
    'Subscribe' => 'Subscribe to our mailing list',
    'SubscribeDescription' => 'We will send you the latest offers, exclusive opportunities and all that matters to investors (our messages will not be annoying or have fake or unimportant content)',
    'SubscribeNow' => 'Subscribe',
    'our_location' => 'Our Location',
    'ContactUs' => 'Contact Us',
    'full_name' => 'Full Name',
    'email' => 'Email',
    'phone' => 'Phone Number',
    'message' => 'Message',
    'categories' => 'Categories',
    'Contact_consultant' => 'Contact your consultant real estate',
    'Privacy_Policy' => 'Privacy Policy',
    'send' => 'Send',
    'Enter_Your_Email' => 'Enter Your Email',
    'Pay_Cash' => 'Pay Cash',
    'no_data' => 'No matching data ',
    'show_other_categories' => 'Please choose another category from the following categories ',
    "testimonials" => "Testimonials",
    "testimonials_description"=>"We trust our customers and take care of their recommendations",
    "service_1" => "Wide portfolio",
    "service_1_description" => "Dimora Property is a place where you can come to for any property related needs within the Turkish borders. We deal with property around the country including cities like in Bursa, Bodrum, Yalova, Fethiye, Antalya and Istanbul.",
    "service_2" => "Years of experience",
    "service_2_description" => "Our expertise range from investment in residential properties to commercial properties. We can give you expert advice regarding what the best course of action would be for you according to your needs. We pride that we always give priority to our clients. ",
    "service_3" => "A to Z services",
    "service_3_description" => "From initial consultation about the best course of action for you, we will make sure we are with you every step of the way. At Dimora we want to make sure every client we have walks out with the best possible outcome for them.",
    "counter_1" => "New Visiters Every Week",
    "counter_2" => "Happy customers every year",
    "counter_3" => "Won Awards",
    "counter_4" => "New Listing Every Week",
    "about_us_title" => "Our Awesome <span>Story</span>",
    "about_us_description" => "We at Dimora are dedicated to giving our clients the best possible advice and guidance that they could need. Our work in a way empowers us to help change peoples’ lives and that is not a responsibility that we take lightly.
                            We have an array of services that we provide to our clients but when duty calls, we are not hesitant in going above and beyond what needs to be done. Whether it’s a consultation about your various options on what you can do in terms of investment into Turkey or if it’s finalizing a property in Turkey that you are interested in; we can help with it all!
                            We have had a lot of success in the past, and only hope to increase it in the future. We do not count success just for us but also when our clients are happy with their investment and association with our business.
                            This is possibly why we have found as much success in the little time we have been operational. Because we always put our clients as our priority and make sure everyone who walks away from the negotiation walks away happy.
                            Our main concerns are property investments within Turkey. We deal in property across the country, namely in Bodrum, Busra, Yalova, Fethiya, Antalya and Istanbul. We are trying to make sure that we cover every city across Turkey so that we can service our clients even better.
                            We have been in the property business for years and have accumulated a lot of experience through the years which can be useful in your search for a good investment opportunity. We have expertise in not only investment opportunities in Turkey but can also guide you about how to go about obtaining citizenship in the country through different investment channels.
                            A lot of businesses will try and sell you something without thinking about what would benefit you the most. We try to keep your best interest as our top priority and figure out the best course of action for you according to your case. Your best interest is what drives us to do better! And this is why have a plethora of satisfied customers who came to us for their investment needs.
                            So do not hesitate in contacting us today, to learn more about what your options are in terms of investment and more! We would be happy to help you in whatever way we can.",

    "Contact_your_real_estate_expert" => "Contact your real estate expert",
    "address" => 'Address',
    "contact_us_description" => "We are more than happy to always help out clients or even potential clients with any needs they may present to us. We have expertise regarding all things related to Turkey’s property market and best investment practices which we would be more than happy to share with our clients.
                            We have dozens of people contacting us every day for various queries. And we are happy to help each and every person who contacts us!
                            We have all our contact information listed below.
                            You can call us on:
                            Feel free to leave a message if it is after work hours and no one responds to the phone. Leave your number in the message and we will get back to you as soon as possible.",
    "Get_In_Touch" => "Get In Touch",
    "Send_Message" => "Send Message"
    ];
