<?php

return [
    'results_for' => 'Results for',
    'search_blog' => 'Search in articles',
    'search' => 'Search',
    'Popular_Posts' => 'Popular Posts',
    'Details' => 'Details',
    'Tags' => 'Tags',
    'categories' => 'Categories',
    'project' => [
        'Gallery' => 'Gallery',
        'Details' => 'Details',
        'Amenities' => 'Amenities',
        'Rooms' => 'Rooms',
        'Reviews' => 'Reviews',
        'shear' => 'Shear',
        'type' => 'Type',
        'SizeFrom' => 'Size From',
        'to' => 'To',
        'PriceFrom' => 'Price From',
        'm2' => 'Sq. M',
        'AveragePrice' => 'Average Price',
        'ContactInformation' => 'Contact Information',
        'Address' => 'Address',
        'Phone' => 'Phone',
        'Mail' => 'EMail',
        'booking' => 'Booking',
        'bookingDescription' => 'Send us a message and we will contact you',
        'name' => 'Full Name',
        'description' => 'Description',
        'send' => 'Send',
    ]
];
